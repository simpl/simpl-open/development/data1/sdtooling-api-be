#!/bin/sh

LOG_FILE="/home/simpluser/entrypoint.log"

log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" | tee -a "$LOG_FILE"
}

log "Starting entrypoint script..."


shapes_dir_base="${shapes_dir_base:-/home/simpluser/data}"
shapes_folder="${shapes_folder:-shapes}"
FINAL_DIR="$shapes_dir_base/$shapes_folder"
log "Current user: $(whoami)"
log "Before mkdir - Ownership and permissions of /home/simpluser:"
ls -ld "/home/simpluser"
log "Checking ownership and permissions of $shapes_dir_base before creation of $FINAL_DIR"

if [ -d "$shapes_dir_base" ]; then
    log "Before mkdir - Ownership and permissions of $shapes_dir_base:"
    ls -ld "$shapes_dir_base"
else
    log "$FINAL_DIR does not exist yet."
fi
log "shapes_dir_base is set to: $shapes_dir_base"
log "shapes_folder is set to: $shapes_folder"
log "Final directory for shapes: $FINAL_DIR"


log "Creating directory: $FINAL_DIR"
mkdir -p "$FINAL_DIR" && log "Directory created successfully: $FINAL_DIR"


if [ -d "/home/simpluser/tmp/shapes" ]; then
    log "Listing contents of /home/simpluser/tmp/shapes:"
    ls -la /home/simpluser/tmp/shapes  

    if [ "$(ls -A /home/simpluser/tmp/shapes)" ]; then
        log "Moving /home/simpluser/tmp/shapes to $FINAL_DIR"
        cp -r /home/simpluser/tmp/shapes/* "$FINAL_DIR" && log "Move successful"
        
        log "Listing contents of $FINAL_DIR after move:"
        ls -la "$FINAL_DIR"  
    else
        log "WARNING: /home/simpluser/tmp/shapes exists but is empty!"
    fi
else
    log "WARNING: /home/simpluser/tmp/shapes does not exist!"
fi


log "Entrypoint script execution completed."




