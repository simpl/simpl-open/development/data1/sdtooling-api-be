package eu.europa.ec.simpl.sdtoolingbe.controller.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import eu.europa.ec.simpl.sdtoolingbe.controller.AbstractController;
import eu.europa.ec.simpl.sdtoolingbe.exception.CustomException;
import eu.europa.ec.simpl.sdtoolingbe.logging.LogRequest;
import eu.europa.ec.simpl.sdtoolingbe.service.validationService.ValidationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/api/contract")
@Tag(name = "Contract Controller")
@Log4j2
public class ContractController extends AbstractController {

    private final ValidationService validationService;

    public ContractController(ValidationService validationService) {
        this.validationService = validationService;
    }

    @LogRequest
    @Operation(
        summary = "Return json validated", description = "Return the json-ld validated", responses = {
            @ApiResponse(
                responseCode = "200", description = "Successfully validated SD JSON-LD", content = @Content(
                    mediaType = "application/json", schema = @Schema(
                        implementation = Object.class
                    ), examples = @ExampleObject(
                        name = "SD JSON-LD validated", value = "Json file validated"
                    )
                )
            )
        }
    )
    @PostMapping(
        path = "/v1/validate", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> contractValidate(
        @Parameter(description = "SD json file") @RequestParam("json-file") MultipartFile jsonFile,
        @Schema(
            requiredMode = Schema.RequiredMode.REQUIRED, description = "Ecosystem name", example = "simpl"
        ) @RequestParam String ecosystem,
        @Schema(
            requiredMode = Schema.RequiredMode.REQUIRED, description = "File name", example = "contract-templateShape.ttl"
        ) @RequestParam String shapeFileName
    ) throws CustomException, IOException {

        try (InputStream inputStream = jsonFile.getInputStream()) {
            String sdJsonLd = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);

            validationService.validateJsonLd(
                sdJsonLd,
                shapeFileName,
                ecosystem
            );

            return new ResponseEntity<>(sdJsonLd, HttpStatus.OK);
        }
    }
}
