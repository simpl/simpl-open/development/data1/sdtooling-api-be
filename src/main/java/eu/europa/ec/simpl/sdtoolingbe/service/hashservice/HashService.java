package eu.europa.ec.simpl.sdtoolingbe.service.hashservice;

public interface HashService {

    /**
     * Generates hash fields and inserts them into the JSON-LD file
     *
     * @param jsonLd    string json file
     * @param ecosystem string indicates the ecosystem selected
     * @return Serialization of the result in JSON-LD format
     */
    String generateHashFromJsonLd(String jsonLd, String ecosystem) throws Exception;

}
