package eu.europa.ec.simpl.sdtoolingbe.controller.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.europa.ec.simpl.sdtoolingbe.controller.AbstractController;
import eu.europa.ec.simpl.sdtoolingbe.enumeration.ErrorCode;
import eu.europa.ec.simpl.sdtoolingbe.exception.BadRequestException;
import eu.europa.ec.simpl.sdtoolingbe.logging.LogRequest;
import eu.europa.ec.simpl.sdtoolingbe.service.conversionservice.ConversionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;

/**
 * Main entry point for REST calls. Provides access to pre-defined files as well as a Turtle to JSON preprocessor for
 * the frontend.
 */
@RestController
@Log4j2
@Tag(name = "Conversion Controller")
public class ConversionController extends AbstractController {

    private static final String ERRROR_MESSAGE_FILE_NOT_FOUND = "File not found";
    private static final String ERRROR_MESSAGE_INPUT_PARAMS_NO_SPECIFIED = "Input params no specified";

    private final ConversionService conversionService;

    public ConversionController(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    /**
     * Returns all available predefined SHACL files. All *.ttl files same directory will be considered.
     *
     * @return a map of file names of all available SHACL files
     */
    @LogRequest
    @GetMapping(value = "/getAvailableShapesTtl")
    @Operation(
        summary = "Get all available shapes ttl file", description = "Return all available shapes ttl file"
    )
    public Map<String, Map<String, List<String>>> getAvailableShapesTtl() {
        log.debug("getAvailableShapesTtl(): invoking conversionService.getAvailableShapesTtl()");
        return conversionService.getAvailableShapesTtl();
    }

    /**
     * To be used after /getAvailableShapesTtl. Returns the content of a predefined TTL file.
     *
     * @param ecosystem the ecosystem of the searched file.
     * @param fileName  the file name to return. Can be chosen from /getAvailableShapes response.
     * @return the full content of the respective TTL file.
     */
    @LogRequest
    @GetMapping(value = "/getTTL")
    @Operation(
        summary = "Get ttl schema file based on ecosystem and file name", description = "Return the file shape ttl if present"
    )
    public ResponseEntity<String> getTTL(
        @Schema(
            requiredMode = Schema.RequiredMode.REQUIRED, description = "Ecosystem name", example = "simpl"
        ) String ecosystem,
        @Schema(
            requiredMode = Schema.RequiredMode.REQUIRED, description = "File name", example = "data-offeringShape.ttl"
        ) String fileName
    ) throws Exception {

        if (ecosystem == null || fileName == null || ecosystem.isEmpty() || fileName.isEmpty()) {
            log.error(ERRROR_MESSAGE_INPUT_PARAMS_NO_SPECIFIED);
            throw new BadRequestException(
                ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), ERRROR_MESSAGE_INPUT_PARAMS_NO_SPECIFIED
            );
        }

        try {
            String s = conversionService.getTTL(ecosystem, fileName);
            return new ResponseEntity<>(s, HttpStatus.OK);
        } catch (IOException e) {
            log.error("File not found: {} ", e.getMessage());
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), ERRROR_MESSAGE_FILE_NOT_FOUND);
        } catch (IllegalArgumentException e) {
            log.error("Illegal file path: {}", e.getMessage());
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), "Illegal file path");
        }
    }

    /**
     * Get all the shapes of one specific ecosystem in ttl format.
     *
     * @param ecosystem shacl files will be categorized by category of ecosystem.
     * @return a map of file names of all available SHACL files for specific ecosystem.
     */
    @LogRequest
    @GetMapping(value = "/getAvailableShapesTtlCategorized")
    @Operation(
        summary = "Get all available shapes ttl file based on ecosystem", description = "Return all available shapes ttl file based on ecosystem"
    )
    public ResponseEntity<Map<String, List<String>>> getAvailableShapesTtlCategorized(
        @Schema(
            requiredMode = Schema.RequiredMode.REQUIRED, description = "Ecosystem name", example = "simpl"
        ) String ecosystem
    ) throws BadRequestException {
        Map<String, List<String>> availableShapes = conversionService.getAvailableShapesTtl(ecosystem);
        if (!availableShapes.isEmpty()) {
            return new ResponseEntity<>(availableShapes, HttpStatus.OK);
        } else {
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage());
        }
    }

}
