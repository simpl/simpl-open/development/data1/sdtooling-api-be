package eu.europa.ec.simpl.sdtoolingbe.service.conversionservice;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import eu.europa.ec.simpl.sdtoolingbe.exception.BadRequestException;

/**
 * 'convertToJson' is the primary method called from the controller class. It
 * converts an input multipart shacl file into a json object for the frontend.
 */
public interface ConversionService {

    
    String getTTL(String ecosystem, String name) throws IOException, BadRequestException;

    Map<String, Map<String, List<String>>> getAvailableShapesTtl();

    Map<String, List<String>> getAvailableShapesTtl(String ecosystem);

}
