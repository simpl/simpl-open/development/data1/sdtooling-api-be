package eu.europa.ec.simpl.sdtoolingbe.logging;

import static eu.simpl.MessageBuilder.buildMessage;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import eu.simpl.types.HttpLogMessage;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Aspect for logging execution of service and repository Spring components.
 */
@Aspect
@Component
public class LoggingAspect {

    private final ThreadLocal<Long> startTime = new ThreadLocal<>();

    private void logObject(JoinPoint joinPoint, LogRequest logRequest, Throwable ex) {
        Class<?> targetClass = joinPoint.getTarget().getClass();

        Logger logger = LogManager.getLogger(targetClass);

        try {

            if (ex != null) {
                logger.error(ex.getMessage());
            } else {

                if (RequestContextHolder.getRequestAttributes() == null) {
                    return;
                }

                long endTime = System.currentTimeMillis();
                long elapsedTime = endTime - startTime.get();

                HttpServletRequest request = ((ServletRequestAttributes) Objects
                    .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();

                HttpServletResponse httpResponse = ((ServletRequestAttributes) Objects
                    .requireNonNull(RequestContextHolder.getRequestAttributes())).getResponse();
                logger.debug("logObject(): response {}", httpResponse);
                Integer requestSize = (Integer) request.getAttribute(RequestSizeFilter.REQUEST_SIZE_ATTRIBUTE);

                MethodSignature ms = (MethodSignature) joinPoint.getSignature();
                logger.debug("logObject(): signature {}", ms);

                String httpRequestSize;
                if (requestSize != null && requestSize != -1) {
                    httpRequestSize = requestSize + " bytes";
                } else {
                    httpRequestSize = "N/A";
                }
                
                String user = "ANONYMOUS";
                String msg = "HTTP request [" + request.getMethod() + "]" + " " + request.getRequestURL();

                if (logRequest.httpStatus() == HttpStatus.OK) {
                    logger.info(
                        buildMessage(
                            HttpLogMessage.builder()
                                .msg(msg)
                                .httpStatus(String.valueOf(logRequest.httpStatus().value()))
                                .httpRequestSize(httpRequestSize)
                                .httpExecutionTime(elapsedTime + " ms")
                                .user(user)
                                .build()
                        )
                    );
                } else {
                    logger.error(
                        buildMessage(
                            HttpLogMessage.builder()
                                .msg(msg)
                                .httpStatus(String.valueOf(logRequest.httpStatus().value()))
                                .httpRequestSize(httpRequestSize)
                                .httpExecutionTime(elapsedTime + " ms")
                                .user(user)
                                .build()
                        )
                    );
                }
            }

        } catch (Exception e) {
            logger.error("logObject() failed cause {}", e.toString());
        } finally {
            startTime.remove();
        }
    }

    @Around("@annotation(logRequest)")
    public Object trace(ProceedingJoinPoint pjp, LogRequest logRequest) throws Throwable {
        startTime.set(System.currentTimeMillis());

        Object returnValue = pjp.proceed();
        logObject(pjp, logRequest, null);

        return returnValue;
    }

    @AfterThrowing(pointcut = "@annotation(logRequest)", throwing = "ex")
    public void logException(JoinPoint joinPoint, LogRequest logRequest, Throwable ex) {
        logObject(joinPoint, logRequest, ex);
    }

}
