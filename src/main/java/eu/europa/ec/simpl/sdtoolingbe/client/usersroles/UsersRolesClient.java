package eu.europa.ec.simpl.sdtoolingbe.client.usersroles;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "usersRolesClient", url = "${users-roles.api-url}")
public interface UsersRolesClient {

    @GetMapping("/agent/identity-attributes")
    String getAgentIdentityAttributes(
        @RequestHeader("Authorization") String bearerToken
    );

    @GetMapping("/credential/download")
    String getCredential(
        @RequestHeader("Authorization") String bearerToken
    );

}
