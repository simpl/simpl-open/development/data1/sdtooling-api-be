package eu.europa.ec.simpl.sdtoolingbe.model.edc.management;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Context {

    @JsonProperty("@vocab")
    private final String vocab;

}
