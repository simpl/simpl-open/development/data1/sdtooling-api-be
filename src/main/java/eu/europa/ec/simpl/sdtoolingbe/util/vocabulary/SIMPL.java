package eu.europa.ec.simpl.sdtoolingbe.util.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;

public final class SIMPL {

    private static final String BASE_URI = "http://w3id.org/gaia-x/";
    private static final String PREFIX = "simpl#";

    public static final Property configure = ResourceFactory.createProperty(BASE_URI + PREFIX + "configure");

    private SIMPL() {
    }
}
