package eu.europa.ec.simpl.sdtoolingbe.service.validationService;

import java.io.IOException;
import java.net.URI;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.europa.ec.simpl.sdtoolingbe.client.ValidationClient;
import eu.europa.ec.simpl.sdtoolingbe.enumeration.ErrorCode;
import eu.europa.ec.simpl.sdtoolingbe.exception.CustomException;
import eu.europa.ec.simpl.sdtoolingbe.properties.ValidationProperties;
import eu.europa.ec.simpl.sdtoolingbe.util.ShaclFileUtil;
import eu.europa.ec.simpl.sdtoolingbe.util.web.CustomMultipartFile;
import feign.FeignException;
import feign.RetryableException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ValidationServiceImpl implements ValidationService {

    private static final String VALIDATION_DISABLED_MESSAGE = "The validation service call is disabled";

    private final ValidationClient validationClient;
    private final ValidationProperties validationProperties;

    public ValidationServiceImpl(ValidationClient validationClient, ValidationProperties validationProperties) {
        this.validationClient = validationClient;
        this.validationProperties = validationProperties;
    }

    @Override
    public void validateJsonLd(String jsonLdFileString, String shapeFileName, String ecosystem) throws CustomException {

        if (!validationProperties.isEnabled()) {
            log.warn(VALIDATION_DISABLED_MESSAGE);
            return;
        }

        String method = "Method: validateJsonLd - ";
        try {
            URI uri = URI.create(validationProperties.getValidationJsonldUrl());
            logRequestData(method, uri, jsonLdFileString, shapeFileName, ecosystem);

            MultipartFile jsonLdFile = null;
            MultipartFile shapeFile = null;

            jsonLdFile = CustomMultipartFile.convertStringWriterToMultipartFile(jsonLdFileString, "jsonLdFile.json");

            String shapeFileString = ShaclFileUtil.getTtlFromFilename(ecosystem, shapeFileName);
            shapeFile = CustomMultipartFile.convertStringWriterToMultipartFile(shapeFileString, "shapeFile.ttl");

            ResponseEntity<String> response = validationClient.validateJsonLd(uri, jsonLdFile, shapeFile, ecosystem);

            handleResponse(method, response, ecosystem);

        } catch (RetryableException e) {
            handleRetryableException(method, e);
        } catch (FeignException e) {
            handleFeignException(method, e);
        } catch (JsonProcessingException e) {
            handleJsonProcessingException(method, e);
        } catch (IOException e) {
            handleIOException(method, e);
        } catch (CustomException e) {
            handleCustomException(method, e);
        }
    }

    private void logRequestData(String method, URI uri, String file, String shapeFile, String ecosystem) {
        log.info("{}Init", method);
        log.info("{}Request uri - {}", method, uri);
        log.info("{}Request inputFile - {}", method, file);
        log.info("{}Request shapeFile - {}", method, shapeFile);
        log.info("{}Request ecosystem - {}", method, ecosystem);
    }

    private void handleResponse(String method, ResponseEntity<String> response, String ecosystem)
        throws CustomException, JsonProcessingException {
        if (response != null) {
            log.info("{}Response Code - {}", method, response.getStatusCode());
            log.info("{}Response: {}", method, response);
            String responseString = response.getBody();
            if (responseString != null) {
                processGraphNode(method, responseString, ecosystem);
            }
        }
    }

    private void processGraphNode(String method, String responseString, String ecosystem)
        throws JsonProcessingException, CustomException {
        log.info("{}processGraphNode", method);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(responseString);
        JsonNode graphNode = jsonNode.get("@graph");

        if (graphNode.isArray()) {
            for (JsonNode node : graphNode) {
                processSingleNode(method, node, ecosystem);
            }
        }
    }

    private void processSingleNode(String method, JsonNode node, String ecosystem) throws CustomException {
        log.info("{}processSingleNode", method);
        String errorDescription = getNodeText(node);
        String errorField = getErrorField(node, ecosystem);
        String valueField = getNodeValue(node);

        if (errorDescription != null && errorField != null) {
            if (valueField != null && !valueField.startsWith("_")) {
                errorDescription = errorDescription + " [" + errorField + "=" + valueField + "]";
            }
            throw new CustomException(ErrorCode.VALIDATION_ERROR.getKeyErrorMessage(), errorField, errorDescription);
        }
    }

    private static String getErrorField(JsonNode node, String ecosystem) {
        JsonNode errorFieldNode = node.get("sh:resultPath");
        if (errorFieldNode != null && errorFieldNode.get("@id") != null) {
            String errorField = errorFieldNode.get("@id").asText();
            return errorField.substring((ecosystem + ":").length());
        }
        return null;
    }

    private static String getNodeValue(JsonNode node) {
        JsonNode valueNode = node.get("sh:value");
        if (valueNode != null && valueNode.get("@value") != null) {
            return valueNode.get("@value").asText();
        } else {
            return null;
        }
    }

    private static String getNodeText(JsonNode node) {
        JsonNode textNode = node.get("sh:resultMessage");
        if (textNode != null) {
            return textNode.asText();
        } else {
            return null;
        }
    }

    private void handleRetryableException(String method, RetryableException exception) {
        logException(method, "{}RetryableException - {}", exception);
    }

    private void handleFeignException(String method, FeignException exception) {
        logException(method, "{}FeignException - {}", exception);
    }

    private void handleJsonProcessingException(String method, JsonProcessingException exception) {
        logException(method, "{}JsonProcessingException - {}", exception);
    }

    private void handleIOException(String method, IOException exception) {
        logException(method, "{}IOException - {}", exception);
    }

    private void handleCustomException(String method, CustomException customException) {
        logException(method, "{}CustomException - {}", customException);
        throw customException;
    }

    private void logException(String method, String exceptionTitle, Exception exception) {
        log.error("{}{}{}", method, exceptionTitle, exception.getMessage());
        log.error(exception);
    }

}
