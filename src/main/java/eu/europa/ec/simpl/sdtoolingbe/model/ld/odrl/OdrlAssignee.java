package eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OdrlAssignee extends OdrlParty {

    public OdrlAssignee(String uid) {
        super(uid, Odrl.ODRL_NS_PREFIX_V2 + "assignee");
    }

}
