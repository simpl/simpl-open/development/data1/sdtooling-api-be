package eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl;

public final class OdrlOperand {

    public static final String COUNT = Odrl.ODRL_NS_PREFIX_V2 + "count";
    public static final String DATETIME = Odrl.ODRL_NS_PREFIX_V2 + "dateTime";
    public static final String DELETION = Odrl.ODRL_NS_PREFIX_V2 + "deletion";

    private OdrlOperand() {
    }

}
