package eu.europa.ec.simpl.sdtoolingbe.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.security.SecurityScheme.In;
import io.swagger.v3.oas.models.security.SecurityScheme.Type;
import io.swagger.v3.oas.models.servers.Server;

@Configuration
public class OpenApiConfig {

    private static final String TITLE = "SD Tooling Application API";
    
    private static final String VERSION = "1.0";
    private static final String DESCRIPTION = "OpenApi documentation for the " + TITLE;
    private static final String BEARER_AUTH = "bearerAuth";

    @Value("${openapi-config.servers}")
    private List<String> servers;

    @Bean
    OpenAPI openAPI() {
        Info info = new Info()
            .title(TITLE)
            .description(DESCRIPTION)
            .version(VERSION)
        ;
        SecurityRequirement securityRequirement = new SecurityRequirement()
            .addList(BEARER_AUTH)
        ;
        List<Server> serversList = servers.stream().map(url -> new Server().url(url)).toList();
        SecurityScheme securityScheme = new SecurityScheme()
                .name(BEARER_AUTH)
                .description("IAA cloud gateway JWT token")
                .scheme("bearer")
                .type(Type.HTTP)
                .bearerFormat("JWT")
                .in(In.HEADER)
        ;
        return new OpenAPI()
            .info(info)
            .addSecurityItem(securityRequirement)
            .servers(serversList)
            .schemaRequirement(BEARER_AUTH, securityScheme)
        ;
    }

}
