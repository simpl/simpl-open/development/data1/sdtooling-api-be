package eu.europa.ec.simpl.sdtoolingbe.util.web;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class CustomMultipartFile implements MultipartFile {

    private final String name;
    private final String originalFilename;
    private final String contentType;
    private final byte[] content;

    public CustomMultipartFile(String name, String originalFilename, String contentType, byte[] content) {
        this.name = name;
        this.originalFilename = originalFilename;
        this.contentType = contentType;
        this.content = content;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getOriginalFilename() {
        return originalFilename;
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public boolean isEmpty() {
        return content == null || content.length == 0;
    }

    @Override
    public long getSize() {
        return content.length;
    }

    @Override
    public byte[] getBytes() throws IOException {
        return content;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(content);
    }

    @Override
    public void transferTo(java.io.File dest) throws IOException, IllegalStateException {
        throw new UnsupportedOperationException("transferTo not supported");
    }

    public static MultipartFile convertStringWriterToMultipartFile(String content, String fileName) {
        if (StringUtils.isBlank(content) || StringUtils.isBlank(fileName)) {
            return new CustomMultipartFile("", "", "multipart/form-data", new byte[0]);
        }
        // Converte la stringa in un array di byte
        byte[] contentBytes = content.getBytes();

        // Crea un oggetto MultipartFile utilizzando CustomMultipartFile
        return new CustomMultipartFile(fileName, fileName, "multipart/form-data", contentBytes);
    }
}
