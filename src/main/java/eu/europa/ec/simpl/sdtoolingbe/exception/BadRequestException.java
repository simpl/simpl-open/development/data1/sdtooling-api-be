package eu.europa.ec.simpl.sdtoolingbe.exception;

public class BadRequestException extends Exception {

    private final String code;

    public BadRequestException(String message) {
        this(null, message);
    }

    public BadRequestException(String code, String message) {
        super(message);
        this.code = code;
    }
    
    public BadRequestException(String code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
