package eu.europa.ec.simpl.sdtoolingbe.service.sdservice;

public interface SDService {

    /**
     * Generates hash fields and inserts them into the JSON-LD file
     *
     * @param jsonLd        string json file
     * @param ecosystem     string indicates the ecosystem selected
     * @param shapeFileName string indicates the shapeFileName
     * @return Serialization of the result in JSON-LD format
     */
    String setOfferingType(String jsonLd, String ecosystem, String shapeFileName) throws Exception;

}
