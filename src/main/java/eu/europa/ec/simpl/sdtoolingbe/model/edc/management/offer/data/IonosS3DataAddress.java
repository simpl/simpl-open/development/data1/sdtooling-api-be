package eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.data;

import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.DataAddress;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class IonosS3DataAddress extends DataAddress {

    public static final String TYPE_NAME = "IonosS3";

    @NotBlank(message = "region must be valorized")
    // region of the provider S3 bucket (ex: de)
    private String region;

    @NotBlank(message = "storage must be valorized")
    // address of the provider S3 storage
    private String storage;

    @NotBlank(message = "bucketName must be valorized")
    // name of the provider S3 bucket
    private String bucketName;

    @NotBlank(message = "blobName must be valorized")
    // name of the asset object (file) present on the provider S3 bucket
    private String blobName;

    @NotBlank(message = "keyName must be valorized")
    // key name
    private String keyName; 

    /*
     * Can be null if already configured in the EDC connector throug ENV variables: EDC_IONOS_ACCESS_KEY,
     * EDC_IONOS_SECRET_KEY
     */
    // access key of the provider S3 storage
    private String accessKey;
    // secret key of the provider S3 storage
    private String secretKey;

    public IonosS3DataAddress() {
        super(TYPE_NAME);
    }

    public IonosS3DataAddress(String region, String storage, String bucketName, String blobName, String keyName) {
        super(TYPE_NAME);
        this.region = region;
        this.storage = storage;
        this.bucketName = bucketName;
        this.blobName = blobName;
        this.keyName = keyName;
    }

}
