package eu.europa.ec.simpl.sdtoolingbe.controller.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.europa.ec.simpl.sdtoolingbe.controller.AbstractController;
import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import eu.europa.ec.simpl.sdtoolingbe.logging.LogRequest;
import eu.europa.ec.simpl.sdtoolingbe.model.client.accesspolicy.AccessPolicyRequest;
import eu.europa.ec.simpl.sdtoolingbe.model.client.identityattribute.IdentityAttribute;
import eu.europa.ec.simpl.sdtoolingbe.model.client.policyaction.PolicyAction;
import eu.europa.ec.simpl.sdtoolingbe.model.client.usagepolicy.UsagePolicyRequest;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlPolicy;
import eu.europa.ec.simpl.sdtoolingbe.service.policy.PolicyService;
import eu.europa.ec.simpl.sdtoolingbe.service.usersroles.UsersRolesService;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/api/policy")
@Tag(name = "Policy Controller")
@Log4j2
public class PolicyController extends AbstractController {

    private PolicyService policyService;

    private UsersRolesService usersRoles;

    public PolicyController(PolicyService policyService, UsersRolesService usersRoles) {
        this.policyService = policyService;
        this.usersRoles = usersRoles;
    }

    @LogRequest
    @Operation(
        summary = "Get CONSUMER type identity attributes", description = "Returns all the identity attributes assigned to type CONSUMER from the Governance Authority"
    )
    @GetMapping("/identity/attributes")
    public List<IdentityAttribute> getIdentityAttributes(HttpServletRequest request) throws UnauthorizedException {
        List<IdentityAttribute> result;
        String jwtToken = AuthBearerUtil.getBearerValue(request);
        if (jwtToken == null) {
            log.warn("getIdentityAttributes(): no Authorization Bearer provided in the request");
            result = Collections.emptyList();
        } else {
            log.debug(
                "getIdentityAttributes(): invoking policyService.getIdentityAttributes() with JWT token {} and participantType 'CONSUMER'",
                jwtToken
            );
            result = usersRoles.getIdentityAttributes(jwtToken, "CONSUMER");
        }

        log.debug("getIdentityAttributes(): returning '{}'", result);
        return result;
    }

    @LogRequest
    @Operation(
        summary = "Get access policy actions", description = "Returns all possible access policy actions"
    )
    @GetMapping(path = "/access/actions", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PolicyAction> getAccessPolicyActions() {
        log.debug("getAccessPolicyActions(): invoking policyService.getAccessPolicyActions()");
        List<PolicyAction> result = policyService.getAccessPolicyActions();

        log.debug("getAccessPolicyActions(): returning '{}'", result);
        return result;
    }

    @LogRequest
    @Operation(
        summary = "Generate access policy json-ld", description = "Returns the access policy json-ld for the specified input parameters passed as json"
    )
    @PostMapping(path = "/access", produces = MediaType.APPLICATION_JSON_VALUE)
    public OdrlPolicy getAccessPolicyJsonLD(
        @Valid @RequestBody AccessPolicyRequest request, HttpServletRequest httpRequest
    ) throws UnauthorizedException {
        log.debug("getAccessPolicyJsonLD(): invoking policyService.getAccessPolicy() for {}", request);
        OdrlPolicy result = policyService.getAccessPolicy(request);

        log.debug("getAccessPolicyJsonLD(): returning '{}' for request '{}'", result, request);
        return result;
    }

    @LogRequest
    @Operation(
        summary = "Generate usage policy json-ld", description = "Returns the usage policy json-ld for the specified input parameters passed as json"
    )
    @PostMapping(path = "/usage", produces = MediaType.APPLICATION_JSON_VALUE)
    public OdrlPolicy getUsagePolicyJsonLD(
        @Valid @RequestBody UsagePolicyRequest request, HttpServletRequest httpRequest
    ) throws UnauthorizedException {
        log.debug("getUsagePolicyJsonLD(): invoking policyService.getUsagePolicy() for {}", request);
        OdrlPolicy result = policyService.getUsagePolicy(request);

        log.debug("getUsagePolicyJsonLD(): returning '{}' for request '{}'", result, request);
        return result;
    }

}
