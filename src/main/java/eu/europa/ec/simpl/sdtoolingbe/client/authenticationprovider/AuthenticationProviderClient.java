package eu.europa.ec.simpl.sdtoolingbe.client.authenticationprovider;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "authenticationProvider", url = "${authentication-provider.api-url}")
public interface AuthenticationProviderClient {

    @GetMapping("/keypair")
    String getKeypair(
        @RequestHeader("Authorization") String bearerToken
    );

}
