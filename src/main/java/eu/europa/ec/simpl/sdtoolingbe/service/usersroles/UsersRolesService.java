package eu.europa.ec.simpl.sdtoolingbe.service.usersroles;

import java.util.List;

import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import eu.europa.ec.simpl.sdtoolingbe.model.client.identityattribute.IdentityAttribute;

public interface UsersRolesService {

    /**
     * Call the Users & Roles microservice of the participant to get all agent
     * identity attributes assigned to the participantType.
     * 
     * @param bearerToken     is the jwtToken to be used to authorize the call to
     *                        Users & Roles microservice
     * @param participantType
     * @return the list filtered by participantType
     * @throws UnauthorizedException
     */
    List<IdentityAttribute> getIdentityAttributes(String bearerToken, String participantType)
        throws UnauthorizedException;

}
