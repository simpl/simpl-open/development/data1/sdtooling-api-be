package eu.europa.ec.simpl.sdtoolingbe.model.client.usagepolicy;

public enum UsagePolicyAction {

    USE("use");

    private String actionName;

    UsagePolicyAction(String actionName) {
        this.actionName = actionName;
    }

    public String getActionName() {
        return actionName;
    }
}
