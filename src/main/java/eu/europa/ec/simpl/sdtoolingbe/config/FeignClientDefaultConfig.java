package eu.europa.ec.simpl.sdtoolingbe.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;

@Configuration
public class FeignClientDefaultConfig {

    private final Logger.Level loggerLevel;

    public FeignClientDefaultConfig(@Value("${feign.client.config.default.loggerLevel:NONE}") Logger.Level loggerLevel) {
        this.loggerLevel = loggerLevel;
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return loggerLevel;
    }
}
