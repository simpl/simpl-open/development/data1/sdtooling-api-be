package eu.europa.ec.simpl.sdtoolingbe.service.credential;

public class KeyStoreBuildException extends Exception {

    public KeyStoreBuildException(String message, Throwable cause) {
        super(message, cause);
    }

    public KeyStoreBuildException(String message) {
        super(message);
    }

    public KeyStoreBuildException(Throwable cause) {
        super(cause);
    }

}
