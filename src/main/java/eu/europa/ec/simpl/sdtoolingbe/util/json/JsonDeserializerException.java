package eu.europa.ec.simpl.sdtoolingbe.util.json;

import com.fasterxml.jackson.core.JsonProcessingException;

public class JsonDeserializerException extends JsonProcessingException {

    public JsonDeserializerException(String message) {
        super(message);
    }

}
