package eu.europa.ec.simpl.sdtoolingbe.controller.impl;

import eu.europa.ec.simpl.sdtoolingbe.controller.AbstractController;
import eu.europa.ec.simpl.sdtoolingbe.logging.LogRequest;
import eu.europa.ec.simpl.sdtoolingbe.model.status.StatusResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.actuate.health.HealthComponent;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@RestController
@Tag(name = "Status Controller")
@Log4j2
public class StatusController extends AbstractController {
    private static final String PATH_PIPELINE_VARIABLES_FILE = "./pipeline.variables.sh";
    private static final String PROJECT_VERSION_NUMBER = "PROJECT_VERSION_NUMBER";
    private static final int EXPECTED_PARTS_LENGTH = 2;

    private final HealthEndpoint healthEndpoint;

    public StatusController(HealthEndpoint healthEndpoint) {
        this.healthEndpoint = healthEndpoint;
    }

    @LogRequest
    @Operation(
            summary = "Status information",
            description = "Return a json object with the status information of microservice",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully",
                            content = @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Object.class),
                                    examples = @ExampleObject(
                                            """
                                                    {
                                                      "status": "UP",
                                                      "version": "1.0.0"
                                                    }
                                                    """
                                    )
                            )
                    )
            }
    )
    @GetMapping(value = "/status")
    public StatusResponse getStatus() {
        HealthComponent health = healthEndpoint.health();

        Map<String, String> variables = readVariables();

        if (variables.containsKey(PROJECT_VERSION_NUMBER)) {
            String versionValue = variables.get(PROJECT_VERSION_NUMBER);
            log.error("Variable version found {}={}", PROJECT_VERSION_NUMBER, versionValue);
            return new StatusResponse(health.getStatus().getCode(), versionValue);
        } else {
            String error = "Version file not found or unreadable";
            log.error(error);
            return new StatusResponse(health.getStatus().getCode(), error);
        }
    }

    public static Map<String, String> readVariables() {
        Map<String, String> variables = new HashMap<>();

        try (Stream<String> lines = Files.lines(Paths.get(PATH_PIPELINE_VARIABLES_FILE))) {
            lines.filter(line -> line.contains("="))
                    .forEach((String line) -> {
                        String[] parts = line.replace("\"", "").split("=");
                        if (parts.length == EXPECTED_PARTS_LENGTH) {
                            variables.put(parts[0].trim(), parts[1].trim());
                        }
                    });
        } catch (IOException e) {
            log.error("Error reading file: {} {}", PATH_PIPELINE_VARIABLES_FILE, e.getMessage());
        }

        return variables;
    }

}
