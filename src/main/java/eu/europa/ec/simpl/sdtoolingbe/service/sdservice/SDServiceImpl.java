package eu.europa.ec.simpl.sdtoolingbe.service.sdservice;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import eu.europa.ec.simpl.sdtoolingbe.enumeration.ErrorCode;
import eu.europa.ec.simpl.sdtoolingbe.exception.BadRequestException;
import eu.europa.ec.simpl.sdtoolingbe.properties.OfferingTypeProperties;
import lombok.extern.log4j.Log4j2;

/**
 * SDService
 */
@Service
@Log4j2
public class SDServiceImpl implements SDService {

    private static final String LOG_INIT = "Init";

    private static final String SHAPE_OFFERING_TYPE_FIELD_PARENT_NAME = "generalServiceProperties";
    private static final String SHAPE_OFFERING_TYPE_FIELD_NAME = "offeringType";

    private final OfferingTypeProperties offeringTypeProperties;

    public SDServiceImpl(OfferingTypeProperties offeringTypeProperties) {
        this.offeringTypeProperties = offeringTypeProperties;
    }

    @Override
    public String setOfferingType(String jsonLd, String ecosystem, String shapeFileName) throws Exception {
        String method = "Method: generateHashFromJsonLd - ";
        log.info("{}{}", method, LOG_INIT);

        JsonNode jsonFile;
        try {
            jsonFile = new ObjectMapper().readTree(jsonLd);
        } catch (Exception e) {
            log.error("{}Convert jsonLd to JsonNode exception: {}", method, e.getMessage());
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), "Input Json-Ld not valid");
        }
        if (jsonFile == null) {
            log.info("{}Input Json-Ld not valid: {}", method, jsonLd);
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), "Input Json-Ld not valid");
        }

        generateOfferingType(ecosystem, jsonFile, shapeFileName);

        return jsonFile.toString();
    }

    private void generateOfferingType(String ecosystem, JsonNode jsonFile, String shapeFileName) {
        String method = "Method: generateHashJson - ";
        log.info("{}{}", method, LOG_INIT);

        if (jsonFile.has(ecosystem + ":" + SHAPE_OFFERING_TYPE_FIELD_PARENT_NAME)) {
            JsonNode offeringTypeParentObj = jsonFile.get(ecosystem + ":" + SHAPE_OFFERING_TYPE_FIELD_PARENT_NAME);
            log.info("{}Object parent found: {}", method, offeringTypeParentObj);

            ObjectNode offeringTypeObjNew = (ObjectNode) offeringTypeParentObj;

            String offeringTypeValue = offeringTypeProperties.getValueFromKey(shapeFileName);

            if (offeringTypeValue != null) {
                offeringTypeObjNew.put(ecosystem + ":" + SHAPE_OFFERING_TYPE_FIELD_NAME, offeringTypeValue);
                log.info("{}Put offering Type Value: {}", method, offeringTypeValue);
            }
        }
    }

}
