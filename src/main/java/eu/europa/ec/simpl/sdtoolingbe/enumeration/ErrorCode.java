package eu.europa.ec.simpl.sdtoolingbe.enumeration;

public enum ErrorCode {

    GENERAL_ERROR,
    VALIDATION_ERROR,
    DOCUMENT_KEY_NOT_FOUND,
    URL_NOT_FOUND,
    URL_VALUE_NOT_VALID,
    DID_NOT_FOUND,
    INVALID_DOCUMENT,
    EDC_CONNECTOR_UNAVAILABLE,
    ;

    public String getKeyErrorMessage() {
        return this.name();
    }

}
