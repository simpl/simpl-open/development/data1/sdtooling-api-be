package eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl;

public final class OdrlOperator {

    public static final String EQUAL = Odrl.ODRL_NS_PREFIX_V2 + "eq";
    public static final String GREAT = Odrl.ODRL_NS_PREFIX_V2 + "gt";
    public static final String GREAT_OR_EQUAL = Odrl.ODRL_NS_PREFIX_V2 + "gteq";
    public static final String LESS = Odrl.ODRL_NS_PREFIX_V2 + "lt";
    public static final String LESS_OR_EQUAL = Odrl.ODRL_NS_PREFIX_V2 + "lteq";
    public static final String NOT_EQUAL = Odrl.ODRL_NS_PREFIX_V2 + "neq";

    private OdrlOperator() {
    }

}
