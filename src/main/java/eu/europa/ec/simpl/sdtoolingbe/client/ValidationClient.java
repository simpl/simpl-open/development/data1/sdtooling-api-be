package eu.europa.ec.simpl.sdtoolingbe.client;

import java.net.URI;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(value = "validationClient", url = "placeholder")
public interface ValidationClient {

    @PostMapping(consumes = "multipart/form-data")
    ResponseEntity<String> validateTtt(
        URI uri,
        @RequestPart("ttl-file") MultipartFile ttlFile,
        @RequestPart("shape-file") MultipartFile shapeFile,
        @RequestParam("ecosystem") String ecosystem
    );

    @PostMapping(consumes = "multipart/form-data")
    ResponseEntity<String> validateJsonLd(
        URI uri,
        @RequestPart("jsonld-file") MultipartFile jsonLdFile,
        @RequestPart("shape-file") MultipartFile shapeFile,
        @RequestParam("ecosystem") String ecosystem
    );

}
