package eu.europa.ec.simpl.sdtoolingbe.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.europa.ec.simpl.sdtoolingbe.enumeration.ErrorCode;
import eu.europa.ec.simpl.sdtoolingbe.error.BaseResponse;
import eu.europa.ec.simpl.sdtoolingbe.error.ErrorResponse;
import eu.europa.ec.simpl.sdtoolingbe.exception.BadRequestException;
import eu.europa.ec.simpl.sdtoolingbe.exception.CustomException;
import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import eu.europa.ec.simpl.sdtoolingbe.logging.LogRequest;
import eu.europa.ec.simpl.sdtoolingbe.util.ExceptionUtil;
import eu.europa.ec.simpl.sdtoolingbe.util.json.JsonDeserializerException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public abstract class AbstractController {

    protected static final String RESPONSE_BODY = "responseBody";

    protected static final String CODE_BAD_REQUEST = "400";
    protected static final String CODE_UNAUTHORIZED = "401";
    protected static final String CODE_INTERNAL_SERVER_ERROR = "500";
    protected static final String CODE_SERVICE_UNAVAILABLE = "503";

    protected ObjectMapper objectMapper;

    protected AbstractController() {
        objectMapper = new ObjectMapper();
    }

    @Operation(
        responses = {
            @ApiResponse(
                responseCode = CODE_BAD_REQUEST, content = @Content(
                    mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)
                )
            )
        }
    )
    @ExceptionHandler(MissingServletRequestPartException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ApiResponse(
        responseCode = CODE_BAD_REQUEST, content = @Content(
            mediaType = "application/problem+json", schema = @Schema(implementation = ErrorResponse.class)
        )
    )
    @LogRequest(httpStatus = HttpStatus.BAD_REQUEST)
    public BaseResponse<ErrorResponse> missingServletRequestPartException(
        HttpServletRequest request, MissingServletRequestPartException e
    ) {
        log.error("missingServletRequestPartException() for request " + request.getRequestURL(), e);
        String errorDescription = e.getRequestPartName();
        return createFaultResponse(
            request, HttpStatus.BAD_REQUEST, ErrorCode.VALIDATION_ERROR, "Missing argument", errorDescription
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ApiResponse(
        responseCode = CODE_BAD_REQUEST, content = @Content(
            mediaType = "application/problem+json", schema = @Schema(implementation = ErrorResponse.class)
        )
    )
    @LogRequest(httpStatus = HttpStatus.BAD_REQUEST)
    public BaseResponse<ErrorResponse> methodArgumentNotValidException(
        HttpServletRequest request, MethodArgumentNotValidException e
    ) {
        log.error("methodArgumentNotValidException() for request " + request.getRequestURL(), e);
        /*
         * example how to collect all validation errors List<String> errors = ex.getBindingResult().getFieldErrors()
         * .stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList()) ;
         */

        // but I prefer to return just the first validation error as errorDescription
        String errorDescription = e.getBindingResult().getFieldErrors().getFirst().getDefaultMessage();
        return createFaultResponse(
            request, HttpStatus.BAD_REQUEST, ErrorCode.VALIDATION_ERROR, "Invalid payload", errorDescription
        );
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ApiResponse(
        responseCode = CODE_BAD_REQUEST, content = @Content(
            mediaType = "application/problem+json", schema = @Schema(implementation = ErrorResponse.class)
        )
    )
    @LogRequest(httpStatus = HttpStatus.BAD_REQUEST)
    public BaseResponse<ErrorResponse> methodArgumentNotValidException(
        HttpServletRequest request, HttpMessageNotReadableException e
    ) {
        log.error("methodArgumentNotValidException() for request " + request.getRequestURL(), e);
        String errorDescription;
        JsonDeserializerException jsonDeserializerException = ExceptionUtil
            .findCause(e, JsonDeserializerException.class);
        if (jsonDeserializerException != null) {
            errorDescription = jsonDeserializerException.getMessage();
        } else {
            errorDescription = e.getMessage();
        }
        return createFaultResponse(
            request, HttpStatus.BAD_REQUEST, ErrorCode.VALIDATION_ERROR, "Invalid payload", errorDescription
        );
    }

    @ExceptionHandler(CustomException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ApiResponse(
        responseCode = CODE_BAD_REQUEST, content = @Content(
            mediaType = "application/problem+json", schema = @Schema(implementation = ErrorResponse.class)
        )
    )
    @LogRequest(httpStatus = HttpStatus.BAD_REQUEST)
    public BaseResponse<ErrorResponse> hashGenerationException(HttpServletRequest request, CustomException e) {
        log.error("hashGenerationException() for request " + request.getRequestURL(), e);
        return createFaultResponse(
            request, HttpStatus.BAD_REQUEST, e.getCode(), e.getErrorTitle(), e.getMessage()
        );
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ApiResponse(
        responseCode = CODE_BAD_REQUEST, content = @Content(
            mediaType = "application/problem+json", schema = @Schema(implementation = ErrorResponse.class)
        )
    )
    @LogRequest(httpStatus = HttpStatus.BAD_REQUEST)
    public BaseResponse<ErrorResponse> badRequestException(HttpServletRequest request, BadRequestException e) {
        log.error("badRequestException() for request " + request.getRequestURL(), e);
        return createFaultResponse(
            request, HttpStatus.BAD_REQUEST, e.getCode(), null, e.getMessage()
        );
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ApiResponse(
        responseCode = CODE_UNAUTHORIZED, content = @Content(
            mediaType = "application/problem+json", schema = @Schema(implementation = ErrorResponse.class)
        )
    )
    @LogRequest(httpStatus = HttpStatus.UNAUTHORIZED)
    public BaseResponse<ErrorResponse> unauthorizedException(HttpServletRequest request, UnauthorizedException e) {
        log.error("unauthorizedException() for request " + request.getRequestURL(), e);
        return createFaultResponse(
            request, HttpStatus.UNAUTHORIZED, e.getCode(), null, e.getMessage()
        );
    }

    @ExceptionHandler({ Throwable.class })
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ApiResponse(
        responseCode = CODE_INTERNAL_SERVER_ERROR, content = @Content(
            mediaType = "application/problem+json", schema = @Schema(implementation = ErrorResponse.class)
        )
    )
    @LogRequest(httpStatus = HttpStatus.INTERNAL_SERVER_ERROR)
    public BaseResponse<ErrorResponse> generalException(HttpServletRequest request, Throwable t) {
        log.error("generalException() for request " + request.getRequestURL(), t);
        return createFaultResponse(
            request, HttpStatus.INTERNAL_SERVER_ERROR, ErrorCode.GENERAL_ERROR, "Unexpected internal error",
            "Please try again"
        );
    }

    /**
     * @param request          where set RESPONSE_BODY attribute with faultResponse as JSON
     * @param status
     * @param errorCode        to set keyErrorMessage = errorCode.keyErrorMessage
     * @param errorTitle       to set ErrorResponse.errorTile (optional: null) default 'Generic'
     * @param errorDescription to set ErrorResponse.errorDescription
     * @return
     */
    protected BaseResponse<ErrorResponse> createFaultResponse(
        HttpServletRequest request, HttpStatus status, ErrorCode errorCode, String errorTitle, String errorDescription
    ) {
        return createFaultResponse(request, status, errorCode.getKeyErrorMessage(), errorTitle, errorDescription);
    }

    /**
     * @param request          where set RESPONSE_BODY attribute with faultResponse as JSON
     * @param status
     * @param keyErrorMessage
     * @param errorTitle       to set ErrorResponse.errorTile (optional: null) default 'Generic'
     * @param errorDescription to set ErrorResponse.errorDescription
     * @return
     */
    protected BaseResponse<ErrorResponse> createFaultResponse(
        HttpServletRequest request, HttpStatus status, String keyErrorMessage, String errorTitle,
        String errorDescription
    ) {
        BaseResponse<ErrorResponse> faultResponse = new BaseResponse<>();
        faultResponse.setKeyErrorMessage(keyErrorMessage);
        faultResponse.setResponse(new ErrorResponse(errorTitle, errorDescription));
        try {
            request.setAttribute(RESPONSE_BODY, objectMapper.writeValueAsString(faultResponse));
        } catch (Exception e) {
            log.warn("createFaultResponse() failed to set attribute '" + RESPONSE_BODY + "' cause unexpected error", e);
        }
        log.error("returning {} response with fault {}", status, faultResponse);
        return faultResponse;
    }
}
