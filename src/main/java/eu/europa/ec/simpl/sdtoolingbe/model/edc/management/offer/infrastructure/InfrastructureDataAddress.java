package eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.infrastructure;

import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.DataAddress;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class InfrastructureDataAddress extends DataAddress {

    public static final String TYPE_NAME = "Infrastructure";

    // URL
    private final String provisioningAPI;
    // UUID
    private final String deploymentScriptId;

    public InfrastructureDataAddress(String provisioningAPI, String deploymentScriptId) {
        super(TYPE_NAME);
        this.provisioningAPI = provisioningAPI;
        this.deploymentScriptId = deploymentScriptId;
    }

}
