package eu.europa.ec.simpl.sdtoolingbe.service.federtedcatalogue;

import java.security.KeyStore;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.europa.ec.simpl.client.core.ssl.SslInfo;
import eu.europa.ec.simpl.sdtoolingbe.client.federatedcatalog.FederatedCatalogueTier2Client;
import eu.europa.ec.simpl.sdtoolingbe.exception.InvalidSDJsonException;
import eu.europa.ec.simpl.sdtoolingbe.service.credential.CredentialService;
import eu.europa.ec.simpl.sdtoolingbe.util.feign.FeignUtil;
import feign.FeignException;
import feign.Logger;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class FederatedCatalogueServiceImpl implements FederatedCatalogueService {

    @Value("${federated-catalogue.tier2-gateway.uri}")
    private String tier2GatewayUri;

    @Value("${federated-catalogue.tier2-gateway.path-prefix}")
    private String tier2GatewayPathPrefix;

    @Value("${participant.onboarding.authority.tls.uri}")
    private String authorityTLSUri;

    @Value("${feign.client.config.default.loggerLevel}")
    private Logger.Level feignClientLoggerLevel;

    // to get the provider onboarding credentials on the fly (cached)
    private final CredentialService credentialService;

    // to enforce tier2 tls client to handle sdJsonLd String in the body of request
    private ObjectMapper objectMapper = new ObjectMapper();

    public FederatedCatalogueServiceImpl(
        CredentialService credentialService
    ) {
        this.credentialService = credentialService;
    }

    @Override
    public String publishSD(String sdJsonLd, String tier1BearerToken) throws Exception {
        KeyStore keyStore = credentialService.getCredential(tier1BearerToken);

        SslInfo sslInfo = new SslInfo(keyStore);
        FederatedCatalogueTier2Client client = getClient(sslInfo, tier1BearerToken);
        try {
            // Map<String, Object> is necessary to have a plain json string in the request body
            Map<String, Object> sdJsonLdObj = objectMapper
                .readValue(sdJsonLd, new TypeReference<Map<String, Object>>() {
                });
            // Map<String, Object> is necessary to get the json from the response
            Map<String, Object> response = client.selfDescriptions(tier2GatewayPathPrefix, sdJsonLdObj);
            return objectMapper.writeValueAsString(response);
        } catch (FeignException e) {
            if (e instanceof FeignException.Conflict) {
                // 409: self-description with hash .... already exists
                throw new InvalidSDJsonException(e.getMessage());
            }
            log.error("publishSD() failed", e);
            throw e;
        }
    }

    protected FederatedCatalogueTier2Client getClient(SslInfo sslInfo, String federatedCatalogueAccessToken) {
        return FeignUtil.getClient(
            sslInfo, federatedCatalogueAccessToken, authorityTLSUri, tier2GatewayUri,
            feignClientLoggerLevel
        );
    }

}
