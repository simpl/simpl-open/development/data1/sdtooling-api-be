package eu.europa.ec.simpl.sdtoolingbe.model.edc.management;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.data.HttpDataDataAddress;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.data.IonosS3DataAddress;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.infrastructure.InfrastructureDataAddress;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type"
)
@JsonSubTypes(
    {
        @JsonSubTypes.Type(value = HttpDataDataAddress.class, name = HttpDataDataAddress.TYPE_NAME),
        @JsonSubTypes.Type(value = IonosS3DataAddress.class, name = IonosS3DataAddress.TYPE_NAME),
        @JsonSubTypes.Type(value = InfrastructureDataAddress.class, name = InfrastructureDataAddress.TYPE_NAME)
    }
)
@Data
public abstract class DataAddress {

    @NotBlank(message = "type must be valorized")
    private final String type;

}
