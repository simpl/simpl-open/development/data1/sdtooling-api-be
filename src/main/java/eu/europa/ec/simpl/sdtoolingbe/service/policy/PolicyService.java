package eu.europa.ec.simpl.sdtoolingbe.service.policy;

import java.util.List;

import eu.europa.ec.simpl.sdtoolingbe.model.client.accesspolicy.AccessPolicyRequest;
import eu.europa.ec.simpl.sdtoolingbe.model.client.policyaction.PolicyAction;
import eu.europa.ec.simpl.sdtoolingbe.model.client.usagepolicy.UsagePolicyRequest;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlPolicy;

public interface PolicyService {

    List<PolicyAction> getAccessPolicyActions();

    OdrlPolicy getAccessPolicy(AccessPolicyRequest request);

    OdrlPolicy getUsagePolicy(UsagePolicyRequest request);

}
