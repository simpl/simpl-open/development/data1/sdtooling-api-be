package eu.europa.ec.simpl.sdtoolingbe.constant;

public final class Constants {
    
    public static final long CORS_PRE_FLIGHT_MAX_AGE = 3600;
    
    public static final String NODE_KIND = "nodeKind";
    public static final String TTL = "TTL";
    public static final String JSON = "JSON";
    public static final String IRI = "IRI";
    public static final String URL = "URL";
    public static final String NODE = "node";
    public static final String ALIAS = "alias";
    public static final String VALUE = "value";
    public static final String PREFIX = "prefix";
    public static final String MIN_LENGTH = "minLength";
    public static final String MAX_LENGTH = "maxLength";
    public static final String MIN_INCLUSIVE = "minInclusive";
    public static final String MAX_INCLUSIVE = "maxInclusive";
    public static final String MIN_EXCLUSIVE = "minExclusive";
    public static final String MAX_EXCLUSIVE = "maxExclusive";
    
    private Constants() {}
}
