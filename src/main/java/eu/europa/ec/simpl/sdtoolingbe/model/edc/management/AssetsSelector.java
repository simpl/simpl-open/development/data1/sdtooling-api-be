package eu.europa.ec.simpl.sdtoolingbe.model.edc.management;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AssetsSelector {

    @JsonProperty("@type")
    private String type = "CriterionDto";

    private String operandLeft = "https://w3id.org/edc/v0.0.1/ns/id";

    private String operator = "=";

    private String operandRight;

    public AssetsSelector(String id) {
        this.operandRight = id;
    }

}
