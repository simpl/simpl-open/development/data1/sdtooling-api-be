package eu.europa.ec.simpl.sdtoolingbe.exception;

public class InvalidSDJsonException extends Exception {

    public InvalidSDJsonException(String message) {
        super(message);
    }

    public InvalidSDJsonException(Throwable cause) {
        super(cause);
    }

    public InvalidSDJsonException(String message, Throwable cause) {
        super(message, cause);
    }

}
