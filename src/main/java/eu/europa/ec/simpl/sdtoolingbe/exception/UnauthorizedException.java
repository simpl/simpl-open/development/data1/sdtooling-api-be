package eu.europa.ec.simpl.sdtoolingbe.exception;

public class UnauthorizedException extends Exception {

    private final String code;

    public UnauthorizedException(String message) {
        this(message, null);
    }

    public UnauthorizedException(String message, String code) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
