package eu.europa.ec.simpl.sdtoolingbe.util.web;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.auth0.jwt.JWT;

import jakarta.servlet.http.HttpServletRequest;

public final class AuthBearerUtil {

    public static final String BEARER_PREFIX = "Bearer";

    private AuthBearerUtil() {
    }

    public static String getBearerValue(HttpServletRequest request) {
        String authorizationHeader = request.getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            return authorizationHeader.replace("Bearer ", "");
        }
        return null;
    }

    public static String toBearerString(String bearerToken) {
        return BEARER_PREFIX + " " + bearerToken;
    }

    public static JSONObject getJwtTokenPayloadAsJsonObject(HttpServletRequest request) {
        String payload = getJwtTokenPayload(request);
        if (StringUtils.isNotBlank(payload)) {
            return new JSONObject(payload);
        }
        return null;
    }

    public static String getJwtTokenPayload(HttpServletRequest request) {
        String jwtToken = getBearerValue(request);
        if (StringUtils.isNotBlank(jwtToken)) {
            return new String(Base64.decodeBase64(JWT.decode(jwtToken).getPayload()));
        }
        return null;
    }

    public static String getCredentialId(HttpServletRequest request) {
        JSONObject payload = getJwtTokenPayloadAsJsonObject(request);
        if (payload != null) {
            return payload.optString("credential_id", null);
        }
        return null;
    }

}
