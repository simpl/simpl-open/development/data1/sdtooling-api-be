package eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OdrlPolicy extends Odrl {

    /*
     * "@context": "http://www.w3.org/ns/odrl.jsonld", "@type": "Set", "uid": "{uid}", "profile":
     * "http://www.w3.org/ns/odrl/2/odrl.jsonld",
     */

    private String target;

    private OdrlAssigner assigner;

    @JsonProperty("permission")
    private List<OdrlPermission> permissions;

    public OdrlPolicy(String uid, String target) {
        super(uid, "Set");
        this.target = target;
    }

    public OdrlPolicy add(OdrlPermission permission) {
        if (permissions == null) {
            permissions = new ArrayList<>();
        }
        permissions.add(permission);
        return this;
    }

}
