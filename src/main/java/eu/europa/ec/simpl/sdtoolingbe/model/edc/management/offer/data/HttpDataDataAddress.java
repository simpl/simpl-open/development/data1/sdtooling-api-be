package eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.data;

import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.DataAddress;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HttpDataDataAddress extends DataAddress {

    public static final String TYPE_NAME = "HttpData";

    @NotBlank(message = "baseUrl must be valorized")
    private String baseUrl;

    public HttpDataDataAddress() {
        super(TYPE_NAME);
    }

    public HttpDataDataAddress(String baseUrl) {
        super(TYPE_NAME);
        this.baseUrl = baseUrl;
    }

}
