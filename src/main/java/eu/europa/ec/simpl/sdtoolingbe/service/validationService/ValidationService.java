package eu.europa.ec.simpl.sdtoolingbe.service.validationService;

import eu.europa.ec.simpl.sdtoolingbe.exception.CustomException;

public interface ValidationService {

    void validateJsonLd(String jsonLdFileString, String shapeFileName, String ecosystem) throws CustomException;

}
