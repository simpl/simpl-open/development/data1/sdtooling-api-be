package eu.europa.ec.simpl.sdtoolingbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
@EnableFeignClients(basePackages = "eu.europa.ec.simpl.sdtoolingbe")
@ComponentScan(basePackages = "eu.europa.ec.simpl.sdtoolingbe")
public class SdToolingBeApplication {
    public static void main(String[] args) {
        SpringApplication.run(SdToolingBeApplication.class, args);
    }
}
