package eu.europa.ec.simpl.sdtoolingbe.service.conversionservice;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.springframework.stereotype.Service;

import eu.europa.ec.simpl.sdtoolingbe.constant.Constants;
import eu.europa.ec.simpl.sdtoolingbe.enumeration.ErrorCode;
import eu.europa.ec.simpl.sdtoolingbe.exception.BadRequestException;
import eu.europa.ec.simpl.sdtoolingbe.util.ShaclFileUtil;
import jakarta.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;

/**
 * 'convertToJson' is the primary method called from the controller class. It
 * converts an input multipart shacl file into a json object for the frontend.
 */
@Service
@Log4j2
public class ConversionServiceImpl implements ConversionService {

    private Map<String, Map<String, List<String>>> ecosystemToCategoryToShaclFileMap;

    @PostConstruct
    public void init() {
        log.info("init()");
        ecosystemToCategoryToShaclFileMap = createEcosystemToShaclFileMap();
        log.info("init() completed: {}", ecosystemToCategoryToShaclFileMap);
    }
    
    @Override
    public String getTTL(String ecosystem, String filename) throws IOException, BadRequestException {
        Map<String, List<String>> mapping = ecosystemToCategoryToShaclFileMap.get(ecosystem);
        
        if (mapping==null) {
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), "Ecosystem not found");
        }

        String schemaType = null;
        for (Map.Entry<String, List<String>> entry : mapping.entrySet()) {
            if (entry.getValue().contains(filename)) {
                schemaType = entry.getKey();
                break;
            }
        }

        if (schemaType == null) {
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), "File not found");
        }
        
        try {
            return Files.readString(ShaclFileUtil.getShapesFilePath(ecosystem, schemaType, filename));
        } catch (IllegalArgumentException e) {
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), e);
        }
    }
    
    @Override
    public Map<String, Map<String, List<String>>> getAvailableShapesTtl() {
        Map<String, Map<String, List<String>>> result = new HashMap<>();
        if (ecosystemToCategoryToShaclFileMap != null) {
            ecosystemToCategoryToShaclFileMap.forEach((String ecosystem, Map<String, List<String>> categories) -> {
                Map<String, List<String>> categoryWithTtlFilenames = new HashMap<>();
                categories.forEach((String category, List<String> files) -> {
                    List<String> ttlFilename = new ArrayList<>(files);
                    categoryWithTtlFilenames.put(category, ttlFilename);
                });
                result.put(ecosystem, categoryWithTtlFilenames);
            });
        }
        return result;
    }
    
    @Override
    public Map<String, List<String>> getAvailableShapesTtl(String ecosystem) {
        Map<String, List<String>> availableShapesTtl = new HashMap<>();
        if (ecosystemToCategoryToShaclFileMap != null && ecosystemToCategoryToShaclFileMap.containsKey(ecosystem)) {
            ecosystemToCategoryToShaclFileMap.get(ecosystem)
                    .forEach((String category, List<String> files) -> {
                        List<String> ttlFilenames = new ArrayList<>(files);
                        availableShapesTtl.put(category, ttlFilenames);
                    });
        }
        return availableShapesTtl;
    }

    private static Map<String, Map<String, List<String>>> createEcosystemToShaclFileMap() {
        Collection<File> files = FileUtils.listFiles(ShaclFileUtil.getShapesDir(),
                FileFilterUtils.suffixFileFilter(Constants.TTL, IOCase.INSENSITIVE), TrueFileFilter.INSTANCE);

        Map<String, Map<String, List<File>>> filesToCategoryAndEcosystemMap = files.stream().collect(
                Collectors.groupingBy(file -> file.getParentFile().getParentFile().getName(), Collectors.groupingBy(
                        file -> file.getParentFile().getName())));

        Map<String, Map<String, List<String>>> ecosystemToShaclFileMap = new HashMap<>();

        filesToCategoryAndEcosystemMap.forEach((String ecosystem, Map<String, List<File>> categoryMap) -> {
            Map<String, List<String>> filesToCategory = new HashMap<>();
            categoryMap.forEach((String category, List<File> shaclFiles) -> {
                List<String> filenames = new ArrayList<>();
                shaclFiles.forEach(file -> filenames.add(file.getName()));
                filesToCategory.put(category, filenames);
            });

            ecosystemToShaclFileMap.put(ecosystem, filesToCategory);
        });

        return ecosystemToShaclFileMap;
    }

}
