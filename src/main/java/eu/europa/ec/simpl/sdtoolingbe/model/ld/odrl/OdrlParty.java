package eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class OdrlParty {

    private String uid;
    private String role;

    protected OdrlParty(String uid, String role) {
        this.uid = uid;
        this.role = role;
    }

}
