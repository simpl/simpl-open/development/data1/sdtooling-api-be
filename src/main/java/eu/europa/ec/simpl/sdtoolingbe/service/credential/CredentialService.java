package eu.europa.ec.simpl.sdtoolingbe.service.credential;

import java.security.KeyStore;

import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;

public interface CredentialService {

    KeyStore getCredential(String bearerToken) throws UnauthorizedException, KeyStoreBuildException;

}
