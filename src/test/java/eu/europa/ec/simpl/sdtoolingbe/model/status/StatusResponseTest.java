package eu.europa.ec.simpl.sdtoolingbe.model.status;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class StatusResponseTest {

    @Test
    void testStatusResponseCreation() {
        String expectedStatus = "UP";
        String expectedVersion = "1.0.0";

        StatusResponse response = new StatusResponse(expectedStatus, expectedVersion);

        assertEquals(expectedStatus, response.getStatus());
        assertEquals(expectedVersion, response.getVersion());
    }

    @Test
    void testSetterMethods() {

        StatusResponse response = new StatusResponse("DOWN", "0.9.0");

        response.setStatus("UP");
        response.setVersion("1.0.1");

        assertEquals("UP", response.getStatus());
        assertEquals("1.0.1", response.getVersion());
    }

}
