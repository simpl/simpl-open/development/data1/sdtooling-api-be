package eu.europa.ec.simpl.sdtoolingbe.logging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {FilterConfig.class})
@ExtendWith(SpringExtension.class)
class FilterConfigTests {
	@Autowired
	private FilterConfig filterConfig;

	/**
	 * Method under test: {@link FilterConfig#loggingFilter()}
	 */
	@Test
	void testLoggingFilter() {
		// Arrange and Act
		FilterRegistrationBean<RequestSizeFilter> actualLoggingFilterResult = filterConfig.loggingFilter();

		// Assert
		Collection<String> servletNames = actualLoggingFilterResult.getServletNames();
		assertInstanceOf(Set.class, servletNames);
		Collection<ServletRegistrationBean<?>> servletRegistrationBeans = actualLoggingFilterResult
				.getServletRegistrationBeans();
		assertInstanceOf(Set.class, servletRegistrationBeans);
		Collection<String> urlPatterns = actualLoggingFilterResult.getUrlPatterns();
		assertEquals(1, urlPatterns.size());
		assertInstanceOf(Set.class, urlPatterns);
		assertEquals("loggingFilter", actualLoggingFilterResult.getFilterName());
		assertEquals(1, actualLoggingFilterResult.getOrder());
		assertFalse(actualLoggingFilterResult.isMatchAfter());
		assertTrue(urlPatterns.contains("/*"));
		assertTrue(servletNames.isEmpty());
		assertTrue(servletRegistrationBeans.isEmpty());
		assertTrue(actualLoggingFilterResult.getInitParameters().isEmpty());
		assertTrue(actualLoggingFilterResult.isAsyncSupported());
		assertTrue(actualLoggingFilterResult.isEnabled());
	}
}
