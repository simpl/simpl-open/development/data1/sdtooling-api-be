package eu.europa.ec.simpl.sdtoolingbe.config;

import org.junit.jupiter.api.Test;

import feign.Logger;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FeignClientDefaultConfigTests {

    @Test
    void testFeignLoggerLevel() {
        FeignClientDefaultConfig config = new FeignClientDefaultConfig(Logger.Level.FULL);
        
        assertEquals(Logger.Level.FULL, config.feignLoggerLevel());
    }

}