package eu.europa.ec.simpl.sdtoolingbe.service.federatedcatalogue;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.security.KeyStore;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.europa.ec.simpl.client.core.ssl.SslInfo;
import eu.europa.ec.simpl.sdtoolingbe.client.federatedcatalog.FederatedCatalogueTier2Client;
import eu.europa.ec.simpl.sdtoolingbe.service.credential.CredentialService;
import eu.europa.ec.simpl.sdtoolingbe.service.federtedcatalogue.FederatedCatalogueService;
import eu.europa.ec.simpl.sdtoolingbe.service.federtedcatalogue.FederatedCatalogueServiceImpl;
import feign.FeignException;

class FederatedCatalogueServiceTests {

    @Mock
    private CredentialService credentialService;
    
    @Mock
    private FederatedCatalogueTier2Client federatedCatalogueTier2Client;

    private FederatedCatalogueService federatedCatalogueService;
    
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        // to override the getClient() method
        federatedCatalogueService = new FederatedCatalogueServiceMock();
        
    }

    @SuppressWarnings("unchecked")
	@Test
    void testPublishSD() throws Exception {
        String sdJsonLd = "{ \"key\": \"value\" }";
        String tier1AccessToken = "tier1-token";
        String expectedResponse = "{\"result\":\"success\"}";

        // Mock UsersRolesService to return a KeyStore when called
        KeyStore keyStore = mock(KeyStore.class);
        when(credentialService.getCredential(tier1AccessToken)).thenReturn(keyStore);

        // Mock FederatedCatalogueTier2Client behavior
        when(federatedCatalogueTier2Client.selfDescriptions(nullable(String.class), any(Map.class))).thenReturn(objectMapper.readValue(expectedResponse, Map.class));
        
        
        // Call the method
        String result = federatedCatalogueService.publishSD(sdJsonLd, tier1AccessToken);

        // Assert the result
        assertEquals(expectedResponse, result);
    }
    
    @SuppressWarnings("unchecked")
    @Test
    void testPublishSDWithFeignException() throws Exception {
    	String sdJsonLd = "{ \"key\": \"value\" }";
    	String tier1AccessToken = "tier1-token";
    	
    	// Mock UsersRolesService to return a KeyStore when called
        KeyStore keyStore = mock(KeyStore.class);
        when(credentialService.getCredential(tier1AccessToken)).thenReturn(keyStore);
        
        // Mock the behavior to throw a FeignException
        when(federatedCatalogueTier2Client.selfDescriptions(nullable(String.class), any(Map.class))).thenThrow(FeignException.class);
        
        // Call the method and assert that a FeignException is thrown
        assertThrows(FeignException.class, () -> federatedCatalogueService.publishSD(sdJsonLd, tier1AccessToken));
    }


    class FederatedCatalogueServiceMock extends FederatedCatalogueServiceImpl {
    	
        public FederatedCatalogueServiceMock() {
            super(credentialService);
        }
        
        @Override
        protected FederatedCatalogueTier2Client getClient(SslInfo sslInfo, String bearerToken) {
            return federatedCatalogueTier2Client;
        }
    }
}

