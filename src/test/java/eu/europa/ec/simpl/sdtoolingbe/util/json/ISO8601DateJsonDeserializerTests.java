package eu.europa.ec.simpl.sdtoolingbe.util.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

class ISO8601DateJsonDeserializerTests {
	
	@InjectMocks
    private ISO8601DateJsonDeserializer deserializer;

    @Mock
    private JsonParser jsonParser;

    @Mock
    private DeserializationContext deserializationContext;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    void testDeserializeWithValidDateWithoutTimeZone() throws IOException {
        // Prepare
        String validDateWithoutTimeZone = "2011-12-03T10:15:30";
        when(jsonParser.getText()).thenReturn(validDateWithoutTimeZone);
        
        // Execute
        Date result = deserializer.deserialize(jsonParser, deserializationContext);
        
        // Assert
        Instant expectedInstant = Instant.parse("2011-12-03T10:15:30Z");
        assertEquals(Date.from(expectedInstant), result);
    }

    @Test
    void testDeserializeWithValidDateWithTimeZone() throws IOException {
        // Prepare
        String validDateWithTimeZone = "2011-12-03T10:15:30+01:00";
        when(jsonParser.getText()).thenReturn(validDateWithTimeZone);

        // Execute
        Date result = deserializer.deserialize(jsonParser, deserializationContext);

        // Assert
        Instant expectedInstant = Instant.parse("2011-12-03T09:15:30Z"); // GMT equivalent
        assertEquals(Date.from(expectedInstant), result);
    }

    @Test
    void testDeserializeWithInvalidDate() throws IOException {
        // Prepare
        String invalidDate = "invalid-date";
        when(jsonParser.getText()).thenReturn(invalidDate);

        // Execute & Assert
        assertThrows(IOException.class, () -> deserializer.deserialize(jsonParser, deserializationContext));
    }

    @Test
    void testDeserializeWithBlankString() throws IOException {
        // Prepare
        when(jsonParser.getText()).thenReturn("   ");

        // Execute
        Date result = deserializer.deserialize(jsonParser, deserializationContext);

        // Assert
        assertNull(result);
    }
	
}
