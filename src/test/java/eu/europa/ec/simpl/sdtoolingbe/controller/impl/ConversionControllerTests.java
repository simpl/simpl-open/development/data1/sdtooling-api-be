package eu.europa.ec.simpl.sdtoolingbe.controller.impl;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;

import eu.europa.ec.simpl.sdtoolingbe.client.authenticationprovider.AuthenticationProviderClient;
import eu.europa.ec.simpl.sdtoolingbe.client.edcconnector.EDCConnectorClient;
import eu.europa.ec.simpl.sdtoolingbe.client.usersroles.UsersRolesClient;
import eu.europa.ec.simpl.sdtoolingbe.service.validationService.ValidationService;

@WebMvcTest(value = ConversionController.class)
@TestPropertySource(properties = "web.mvc.bearer-token.required=false")
class ConversionControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private AuthenticationProviderClient authenticationProviderClient;

    @MockitoBean
    private EDCConnectorClient edcConnectorClient;

    @MockitoBean
    private UsersRolesClient usersRolesClient;

    @MockitoBean
    private ValidationService validationService;

    @MockitoBean
    private HealthEndpoint healthEndpoint;

    @Test
    void testGetAvailableShapesTtl() throws Exception {
        mockMvc.perform(get("/getAvailableShapesTtl"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNotEmpty())
        ;
    }

    private static Stream<Arguments> GetTTLParameters() {
        return Stream.of(
                Arguments.of("simpl", "data-offeringShape.ttl", 200),
                // invalid input
                Arguments.of("", "", 400),
                // file not found
                Arguments.of("simpl", "notExistentFile.ttl", 400),
                // path traversal
                Arguments.of("../simpl", "../data-offeringShape.ttl", 400)
        );
    }

    @ParameterizedTest
    @MethodSource("GetTTLParameters")
    void testGetTTL(String ecosystem, String fileName, int statusCode) throws Exception {
        mockMvc.perform(get("/getTTL")
                        .param("ecosystem", ecosystem)
                        .param("fileName", fileName))
                .andExpect(status().is(statusCode))
        ;
    }

    private static Stream<Arguments> GetAvailableShapesTtlCategorizedParameters() {
        return Stream.of(
                Arguments.of("simpl", 200),
                Arguments.of("notExistentEcosystem", 400)
        );
    }

    @ParameterizedTest
    @MethodSource("GetAvailableShapesTtlCategorizedParameters")
    void testGetAvailableShapesTtlCategorized(String ecosystem, int statusCode) throws Exception {
        mockMvc.perform(get("/getAvailableShapesTtlCategorized")
                        .param("ecosystem", ecosystem))
                .andExpect(status().is(statusCode)
                );
    }

}
