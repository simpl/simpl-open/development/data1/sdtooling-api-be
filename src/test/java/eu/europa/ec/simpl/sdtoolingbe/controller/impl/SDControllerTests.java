package eu.europa.ec.simpl.sdtoolingbe.controller.impl;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;

import eu.europa.ec.simpl.sdtoolingbe.client.ValidationClient;
import eu.europa.ec.simpl.sdtoolingbe.client.authenticationprovider.AuthenticationProviderClient;
import eu.europa.ec.simpl.sdtoolingbe.client.edcconnector.EDCConnectorClient;
import eu.europa.ec.simpl.sdtoolingbe.client.usersroles.UsersRolesClient;
import eu.europa.ec.simpl.sdtoolingbe.exception.EDCRegistrationException;
import eu.europa.ec.simpl.sdtoolingbe.exception.InvalidSDJsonException;
import eu.europa.ec.simpl.sdtoolingbe.service.edcconnector.EDCConnectorService;
import eu.europa.ec.simpl.sdtoolingbe.service.hashservice.HashService;
import eu.europa.ec.simpl.sdtoolingbe.service.validationService.ValidationService;


@WebMvcTest(value = SDController.class)
@TestPropertySource(properties = "web.mvc.bearer-token.required=false")
class SDControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private AuthenticationProviderClient authenticationProviderClient;

    @MockitoBean
    private ValidationClient validationClient;

    @MockitoBean
    private EDCConnectorClient edcConnectorClient;

    @MockitoBean
    private UsersRolesClient usersRolesClient;

    @MockitoBean
    private ValidationService validationService;

    @MockitoBean
    private EDCConnectorService edcConnectorService;

    @MockitoBean
    private HashService hashService;

    @MockitoBean
    private HealthEndpoint healthEndpoint;

    @Test
    void testEnrichAndValidate() throws Exception {
        String sdJsonLd = "{\"key\": \"value\"}";
        String ecosystem = "simpl";
        String shapeFileName = "any";

        MockMultipartFile jsonFile = new MockMultipartFile(
                "json-file",
                "test.json",
                MediaType.APPLICATION_JSON_VALUE,
                sdJsonLd.getBytes(StandardCharsets.UTF_8)
        );

        when(hashService.generateHashFromJsonLd(anyString(), anyString())).thenReturn(sdJsonLd);
        when(edcConnectorService.register(anyString(), anyString(), anyString())).thenReturn(sdJsonLd);
        doNothing().when(validationService).validateJsonLd(anyString(), anyString(), anyString());

        mockMvc.perform(multipart("/api/sd/enrichAndValidate")
                        .file(jsonFile)
                        .param("ecosystem", ecosystem)
                        .param("shapeFileName", shapeFileName)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                )
                .andExpect(status().isOk())
        ;

    }


    @Test
    void testEnrichAndValidateWithInvalidSDJsonException() throws Exception {
        String sdJsonLd = "{\"key\": \"value\"}";
        String ecosystem = "simpl";
        String shapeFileName = "any";

        MockMultipartFile jsonFile = new MockMultipartFile(
                "json-file",
                "test.json",
                MediaType.APPLICATION_JSON_VALUE,
                sdJsonLd.getBytes(StandardCharsets.UTF_8)
        );

        when(hashService.generateHashFromJsonLd(anyString(), anyString())).thenReturn(sdJsonLd);
        when(edcConnectorService.register(anyString(), anyString(), anyString())).thenThrow(new InvalidSDJsonException("test"));

        mockMvc.perform(multipart("/api/sd/enrichAndValidate")
                        .file(jsonFile)
                        .param("ecosystem", ecosystem)
                        .param("shapeFileName", shapeFileName)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                )
                .andExpect(status().isBadRequest())
        ;
    }

    @Test
    void testEnrichAndValidateWithEDCRegistrationException() throws Exception {
        String sdJsonLd = "{\"key\": \"value\"}";
        String ecosystem = "simpl";
        String shapeFileName = "any";

        MockMultipartFile jsonFile = new MockMultipartFile(
                "json-file",
                "test.json",
                MediaType.APPLICATION_JSON_VALUE,
                sdJsonLd.getBytes(StandardCharsets.UTF_8)
        );

        when(hashService.generateHashFromJsonLd(anyString(), anyString())).thenReturn(sdJsonLd);
        when(edcConnectorService.register(anyString(), anyString(), anyString())).thenThrow(new EDCRegistrationException("test"));

        mockMvc.perform(multipart("/api/sd/enrichAndValidate")
                        .file(jsonFile)
                        .param("ecosystem", ecosystem)
                        .param("shapeFileName", shapeFileName)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                )
                .andExpect(status().is(HttpStatus.SERVICE_UNAVAILABLE.value()))
        ;

    }
}
