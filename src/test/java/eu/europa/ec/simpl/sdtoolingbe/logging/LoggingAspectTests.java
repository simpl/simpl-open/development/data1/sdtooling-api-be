package eu.europa.ec.simpl.sdtoolingbe.logging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ObjectMessage;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


class LoggingAspectTests {
	
	@InjectMocks
	private LoggingAspect loggingAspect;
	
	private MockedStatic<RequestContextHolder> mockStaticRequestContextHolder;
	
	@SuppressWarnings("unchecked")
	@BeforeEach
    void beforeEach() throws Exception {
		MockitoAnnotations.openMocks(this);
		
		// inizializzo i mock statici
		mockStaticRequestContextHolder = Mockito.mockStatic(RequestContextHolder.class);
       
        // uso la reflection per accedere al campo privato startTime di LoggingAspect
        Field startTimeField = LoggingAspect.class.getDeclaredField("startTime");
        startTimeField.setAccessible(true);
        ThreadLocal<Long> startTime = (ThreadLocal<Long>) startTimeField.get(loggingAspect);
        startTime.set(System.currentTimeMillis());
        
    }
	
	@AfterEach
	void afterEach() {
		// chiudo i mock statici
		mockStaticRequestContextHolder.close();
	}
	
	/**
	 * Method under test:
	 * {@link LoggingAspect#trace(ProceedingJoinPoint, LogRequest)}
	 */
	@Test
	void testTrace() throws Throwable {
		// Arrange
		ProceedingJoinPoint pjp = mock(ProceedingJoinPoint.class);
		when(pjp.getTarget()).thenReturn(LoggingAspectTests.class);
		when(pjp.proceed()).thenReturn("Proceed");

		// Act
		Object actualTraceResult = loggingAspect.trace(pjp, mock(LogRequest.class));

		// Assert
		verify(pjp).getTarget();
		verify(pjp).proceed();
		assertEquals("Proceed", actualTraceResult);
	}

	/**
	 * Method under test:
	 * {@link LoggingAspect#logException(JoinPoint, LogRequest, Throwable)}
	 */
	@Test
	void testLogException() {
		// Arrange
		JoinPoint joinPoint = mock(JoinPoint.class);
		when(joinPoint.getTarget()).thenReturn(LoggingAspectTests.class);
		LogRequest logRequest = mock(LogRequest.class);

		// Act
		loggingAspect.logException(joinPoint, logRequest, new Throwable("Test Throwable"));

		// Assert
		verify(joinPoint).getTarget();
	}
	
	@Test
	void testLogExceptionWithLogRequestOK() {
		// Arrange
		JoinPoint joinPoint = mock(JoinPoint.class);
		when(joinPoint.getTarget()).thenReturn(LoggingAspectTests.class);
		
		LogRequest logRequest = mock(LogRequest.class);
		when(logRequest.httpStatus()).thenReturn(HttpStatus.OK);
		
		// Mock per RequestContextHolder e request/response
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod("GET");
        request.setRequestURI("/test");

        MockHttpServletResponse response = new MockHttpServletResponse();
        
        RequestAttributes requestAttributes = new ServletRequestAttributes(request, response);

		mockStaticRequestContextHolder.when(RequestContextHolder::getRequestAttributes).thenReturn(requestAttributes);
		
		// non può essere aperto nel beforeEach
		try(MockedStatic<LogManager> mockLogmanager = Mockito.mockStatic(LogManager.class)) {
			Logger mockLogger = mock(Logger.class);
			when(LogManager.getLogger(Mockito.any(Class.class))).thenReturn(mockLogger);
			
			loggingAspect.logException(joinPoint, logRequest, null);
	
			// verifico che sia stato invocato logger.error()
			verify(mockLogger).info(any(ObjectMessage.class));
		}
	}
	
	@Test
	void testLogExceptionWithResponseNotOK() {
		// Arrange
		JoinPoint joinPoint = mock(JoinPoint.class);
		when(joinPoint.getTarget()).thenReturn("Target");
		
		LogRequest logRequest = mock(LogRequest.class);
		when(logRequest.httpStatus()).thenReturn(HttpStatus.BAD_REQUEST);
		
		// Mock per RequestContextHolder e request/response
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod("GET");
        request.setRequestURI("/test");

        MockHttpServletResponse response = new MockHttpServletResponse();

        RequestAttributes requestAttributes = new ServletRequestAttributes(request, response);

		mockStaticRequestContextHolder.when(RequestContextHolder::getRequestAttributes).thenReturn(requestAttributes);
		
		// non può essere aperto nel beforeEach
		try(MockedStatic<LogManager> mockLogmanager = Mockito.mockStatic(LogManager.class)) {
			Logger mockLogger = mock(Logger.class);
			when(LogManager.getLogger(Mockito.any(Class.class))).thenReturn(mockLogger);
			
			loggingAspect.logException(joinPoint, logRequest, null);
	
			// verifico che sia stato invocato logger.error()
			verify(mockLogger).error(any(ObjectMessage.class));
		}
	}
	
}
