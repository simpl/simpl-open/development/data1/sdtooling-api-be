package eu.europa.ec.simpl.sdtoolingbe.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

class ExceptionUtilTests {

    @Test
    void testFindCauseFound() {
        IllegalArgumentException targetException = new IllegalArgumentException("Target exception");
        RuntimeException wrapper = new RuntimeException("Wrapper exception", targetException);

        IllegalArgumentException result = ExceptionUtil.findCause(wrapper, IllegalArgumentException.class);

        assertNotNull(result);
        assertEquals(targetException, result);
    }

    @Test
    void testFindCauseNotFound() {
        IllegalArgumentException targetException = new IllegalArgumentException("Target exception");
        RuntimeException wrapper = new RuntimeException("Wrapper exception", targetException);

        NullPointerException result = ExceptionUtil.findCause(wrapper, NullPointerException.class);

        assertNull(result);
    }

    @Test
    void testFindCauseNoCause() {
        RuntimeException exception = new RuntimeException("No cause");

        IllegalArgumentException result = ExceptionUtil.findCause(exception, IllegalArgumentException.class);

        assertNull(result);
    }

    @Test
    void testFindCauseMultipleLevels() {
        NullPointerException level3 = new NullPointerException("Level 3 exception");
        IllegalStateException level2 = new IllegalStateException("Level 2 exception", level3);
        RuntimeException level1 = new RuntimeException("Level 1 exception", level2);

        NullPointerException result = ExceptionUtil.findCause(level1, NullPointerException.class);

        assertNotNull(result);
        assertEquals(level3, result);
    }
}
