package eu.europa.ec.simpl.sdtoolingbe.util;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import eu.europa.ec.simpl.sdtoolingbe.dto.HashModel;
import eu.europa.ec.simpl.sdtoolingbe.exception.CustomException;
import eu.europa.ec.simpl.sdtoolingbe.properties.HashProperties;

class HashGeneratorUtilTest {

    @Mock
    private HashProperties hashProperties;

    @InjectMocks
    private HashGeneratorUtil hashGeneratorUtil;

    @Mock
    private HttpURLConnection httpURLConnection;

    @BeforeEach
    void setup() throws IOException {
        MockitoAnnotations.openMocks(this);

        when(hashProperties.getHashAlgorithm()).thenReturn("SHA-256");
        when(hashProperties.getHashModelList()).thenReturn(List.of(new HashModel()));

        hashGeneratorUtil = new HashGeneratorUtil(hashProperties);

        when(httpURLConnection.getResponseCode()).thenReturn(HttpStatus.OK.value());
    }


    @Test
    void testGenerateHashValueDocumentNotFound() {
        assertThrows(CustomException.class, () -> hashGeneratorUtil.generateHashValue(null));
    }


    @Test
    void testGenerateHashValueLinkNull() {
        String document = "testDocument";

        when(hashProperties.getValueFromKey(document)).thenReturn(null);

        assertThrows(CustomException.class, () -> hashGeneratorUtil.generateHashValue(document));
    }


    @Test
    void testGenerateHashValueInvalidURL() {
        String document = "testDocument";

        when(hashProperties.getValueFromKey(document)).thenReturn("invalidUrl");

        assertThrows(CustomException.class, () -> hashGeneratorUtil.generateHashValue(document));
    }

    @Test
    void testGenerateHashValueUnreachableURL() {
        String document = "testDocument";

        when(hashProperties.getValueFromKey(document)).thenReturn("http://unreachable-url.unreachable");

        assertThrows(UnknownHostException.class, () -> hashGeneratorUtil.generateHashValue(document));
    }

    @Test
    void testGenerateHashValueValidURL() throws IOException {
        String document = "testDocument";
        String testUrl = "http://reachable-url.reachable";

        when(hashProperties.getValueFromKey(document)).thenReturn(testUrl);

        try (MockedConstruction<URL> mockedUrl = Mockito.mockConstruction(URL.class,
                (mock, context) -> {
                    when(mock.openStream()).thenReturn(new ByteArrayInputStream("test content".getBytes()));
                    when(mock.openConnection()).thenReturn(httpURLConnection);
                })) {

            when(httpURLConnection.getResponseCode()).thenReturn(HttpStatus.OK.value());
            when(mockedUrl.constructed().getFirst().openConnection()).thenReturn(httpURLConnection);

            String hash = hashGeneratorUtil.generateHashValue(document);

            assertNotNull(hash);
        }

    }

}
