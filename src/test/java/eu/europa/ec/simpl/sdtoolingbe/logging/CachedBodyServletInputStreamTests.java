package eu.europa.ec.simpl.sdtoolingbe.logging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import jakarta.servlet.ReadListener;

@ContextConfiguration(classes = {CachedBodyServletInputStream.class})
@ExtendWith(SpringExtension.class)
class CachedBodyServletInputStreamTests {

	/**
	 * Method under test: {@link CachedBodyServletInputStream#read()}
	 */
	@SuppressWarnings("resource")
	@Test
	void testRead() throws IOException {
		// Cover was unable to create a Spring-specific test for this Spring method.

		// Arrange, Act and Assert
		assertEquals(65, (new CachedBodyServletInputStream("AXAXAXAX".getBytes(StandardCharsets.UTF_8))).read());
	}

	/**
	 * Method under test: {@link CachedBodyServletInputStream#isFinished()}
	 */
	@SuppressWarnings("resource")
	@Test
	void testIsFinished() {
		// Cover was unable to create a Spring-specific test for this Spring method.

		// Arrange, Act and Assert
		assertFalse((new CachedBodyServletInputStream("AXAXAXAX".getBytes(StandardCharsets.UTF_8))).isFinished());
		assertTrue((new CachedBodyServletInputStream(new byte[]{})).isFinished());
	}

	/**
	 * Methods under test:
	 * <ul>
	 *   <li>{@link CachedBodyServletInputStream#setReadListener(ReadListener)}
	 *   <li>{@link CachedBodyServletInputStream#isReady()}
	 * </ul>
	 */
	@SuppressWarnings("resource")
	@Test
	void testGettersAndSetters() {
		// Arrange
		CachedBodyServletInputStream cachedBodyServletInputStream = new CachedBodyServletInputStream(
				"AXAXAXAX".getBytes(StandardCharsets.UTF_8));

		// Act
		cachedBodyServletInputStream.setReadListener(mock(ReadListener.class));

		// Assert that nothing has changed
		assertTrue(cachedBodyServletInputStream.isReady());
	}

}


