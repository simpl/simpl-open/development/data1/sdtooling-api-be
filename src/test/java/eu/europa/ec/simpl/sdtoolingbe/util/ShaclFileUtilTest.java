package eu.europa.ec.simpl.sdtoolingbe.util;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;

class ShaclFileUtilTest {

    @Test
    void testGetShapesDir() {
        File shapesDir = ShaclFileUtil.getShapesDir();
        assertNotNull(shapesDir);
        assertTrue(shapesDir.getPath().endsWith("data" + File.separator + "shapes"));
    }

    @Test
    void testGetShapesDirWithEcosystem() {
        File dir = ShaclFileUtil.getShapesDir("testEcosystem");
        assertNotNull(dir);
        assertTrue(dir.getPath().endsWith("data" + File.separator + "shapes" + File.separator + "testEcosystem"));
    }

    @Test
    void testGetShapesFilePath() {
        Path path = ShaclFileUtil.getShapesFilePath("eco", "type", "file.ttl");
        assertNotNull(path);
        assertTrue(path.toString().endsWith("data" + File.separator + "shapes" + File.separator + "eco" + File.separator + "type" + File.separator + "file.ttl"));
    }

    @Test
    void testGetTtlFromFilename() throws IOException {
        String result = ShaclFileUtil.getTtlFromFilename("simpl", "data-offeringShape.ttl");
        assertNotNull(result);
    }

    @Test
    void testCheckTraversalThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> ShaclFileUtil.checkPathTraversal("..", "test"));
        assertThrows(IllegalArgumentException.class, () -> ShaclFileUtil.checkPathTraversal("/etc/passwd", "test"));
    }

    @Test
    void testCheckTraversalWithNullValue() {
        assertDoesNotThrow(() -> ShaclFileUtil.checkPathTraversal(null, "test"));
    }

}
