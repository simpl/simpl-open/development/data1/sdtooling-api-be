package eu.europa.ec.simpl.sdtoolingbe.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class CustomExceptionExceptionTests {
	
	
	@Test
	void testConstructor() {
		CustomException ex = new CustomException("code", "errorTitle", "errorMessage");
		assertEquals("code", ex.getCode());
		assertEquals("errorTitle", ex.getErrorTitle());
		assertEquals("errorMessage", ex.getMessage());
	}

	@Test
	void testConstructorWithNoMessage() {
		CustomException ex = new CustomException("code", "errorTitle");
		assertEquals("code", ex.getCode());
		assertEquals("errorTitle", ex.getErrorTitle());
	}

	@Test
	void testConstructorWithCause() {
		Throwable t = new Throwable();
		CustomException ex = new CustomException("code", "errorTitle", t);
		assertEquals("code", ex.getCode());
		assertEquals("errorTitle", ex.getErrorTitle());
	}

	@Test
	void testConstructorWithCauseAndMessage() {
		Throwable t = new Throwable();
		CustomException ex = new CustomException("code", "errorTitle", "errorMessage", t);
		assertEquals("code", ex.getCode());
		assertEquals("errorTitle", ex.getErrorTitle());
		assertEquals("errorMessage", ex.getMessage());
	}
	
}
