package eu.europa.ec.simpl.sdtoolingbe.filter;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import eu.europa.ec.simpl.sdtoolingbe.TestSupport;
import eu.europa.ec.simpl.sdtoolingbe.service.jwt.JWTServiceImpl;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@ExtendWith(MockitoExtension.class)
class BearerTokenFilterTests {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain chain;

    //@InjectMocks
    private BearerTokenFilter bearerTokenFilter;

    @BeforeEach
    void setUp() throws Exception {
        bearerTokenFilter = new BearerTokenFilter(new JWTServiceImpl());
        ReflectionTestUtils.setField(bearerTokenFilter, "bearerTokenRequired", true);
        ReflectionTestUtils.setField(bearerTokenFilter, "swaggerUiPath", "/swagger-ui");
    	ReflectionTestUtils.setField(bearerTokenFilter, "apiDocsPath", "/api-docs");
        
        // Configura chain.doFilter() per non fare nulla se eventualmente chiamato (lenient)
        lenient().doNothing().when(chain).doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));
    }
    
    // bearerValue
    private static Stream<Arguments> TestDoFilterParameters() {
		return Stream.of(
			Arguments.of(new Object[] {null}),
			Arguments.of("Basic invalid JWT value")
		);
	}

    @ParameterizedTest
    @MethodSource("TestDoFilterParameters")
    void testDoFilter(String bearerValue) throws IOException, ServletException {
        when(request.getHeader("Authorization")).thenReturn(bearerValue);
        // necessario in questo caso
        when(response.getWriter()).thenReturn(new PrintWriter(new StringWriter()));
        
        bearerTokenFilter.doFilter(request, response, chain);

        // Verifica che lo status sia 401
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    void testDoFilterWithValidBearerToken() throws Exception {
    	String jwtToken = TestSupport.createValidJwt();
    	
    	when(request.getRequestURI()).thenReturn("/not_allowed_uri");
        when(request.getHeader("Authorization")).thenReturn(AuthBearerUtil.toBearerString(jwtToken));

        bearerTokenFilter.doFilter(request, response, chain);

        // Verifica che non siano stati impostati errori nella risposta
        verify(response, never()).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    void testDoFilterWithInvalidBearerToken() throws IOException, ServletException {
    	when(request.getHeader("Authorization")).thenReturn(AuthBearerUtil.toBearerString("invalid-jwt-token"));
    	
    	// necessario in questo caso
        when(response.getWriter()).thenReturn(new PrintWriter(new StringWriter()));
    	
        bearerTokenFilter.doFilter(request, response, chain);

        // Verifica che lo status sia 401 e il messaggio di errore corretto
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);        
    }

    @Test
    void testDoFilterWithBearerTokenNotRequired() throws IOException, ServletException {
    	ReflectionTestUtils.setField(bearerTokenFilter, "bearerTokenRequired", false);
    	
        bearerTokenFilter.doFilter(request, response, chain);

        // Verifica che non siano stati impostati errori nella risposta
        verify(response, never()).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
    
    @Test
    void testDoFilterWithRequestURINull() throws IOException, ServletException {
    	ReflectionTestUtils.setField(bearerTokenFilter, "bearerTokenRequired", true);

        when(request.getRequestURI()).thenReturn(null);
        when(response.getWriter()).thenReturn(new PrintWriter(new StringWriter()));
    	
        bearerTokenFilter.doFilter(request, response, chain);
        
        // Verifica che lo status sia 401 e il messaggio di errore corretto
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
    
    @Test
    void testDoFilterWithTokenAllowedPathsEmpty() throws IOException, ServletException {
    	ReflectionTestUtils.setField(bearerTokenFilter, "bearerTokenRequired", true);
    	ReflectionTestUtils.setField(bearerTokenFilter, "bearerTokenAllowedPaths", Collections.EMPTY_LIST);
    	
        when(request.getRequestURI()).thenReturn("/actuator/health");
        when(response.getWriter()).thenReturn(new PrintWriter(new StringWriter()));
    	
        bearerTokenFilter.doFilter(request, response, chain);
        
        // Verifica che lo status sia 401 e il messaggio di errore corretto
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
    
    
    // requestURI
    private static Stream<Arguments> TestDoFilterWithTokenAllowedPaths() {
		return Stream.of(
			Arguments.of("/swagger-ui", 0),
			Arguments.of("/api-docs", 0),
			Arguments.of("/actuator/health/liveness", 0),
			Arguments.of("/actuator/health/readiness", 0),
			Arguments.of("/test", 0),
			Arguments.of("/test/sub1", 0),
			Arguments.of("/test/sub1/sub2", 0),
			
			Arguments.of("/actuator/health", HttpServletResponse.SC_UNAUTHORIZED),
			Arguments.of("/actuator/health/readiness/sub", HttpServletResponse.SC_UNAUTHORIZED)
			
		);
	}
    
    @ParameterizedTest
    @MethodSource("TestDoFilterWithTokenAllowedPaths")
    void testDoFilterWithTokenAllowedPaths(String requestURI, int expectedStatus) throws IOException, ServletException {
    	ReflectionTestUtils.setField(bearerTokenFilter, "bearerTokenRequired", true);
    	ReflectionTestUtils.setField(bearerTokenFilter, "bearerTokenAllowedPaths", List.of("/actuator/health/*", "/test/**"));
    	
        when(request.getRequestURI()).thenReturn(requestURI);
        lenient().when(response.getWriter()).thenReturn(new PrintWriter(new StringWriter()));
    	
        bearerTokenFilter.doFilter(request, response, chain);
        
        if(expectedStatus!=0) {
        	verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        else {
        	verify(response, never()).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
    
}
