package eu.europa.ec.simpl.sdtoolingbe;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.Odrl;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlAssignee;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlAssigner;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlConstraint;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlOperand;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlOperator;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlPermission;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlPolicy;

public class TestOdrlModel {
	
	protected static ObjectMapper om = new ObjectMapper();

	public static void main(String[] args) throws Exception {
		
		om.enable(SerializationFeature.INDENT_OUTPUT);
		
		OdrlPolicy policy = new OdrlPolicy(UUID.randomUUID().toString(), "request.resourceUri");
		policy.setAssigner(new OdrlAssigner("participant_id_from_aruba"));
		OdrlPermission permission;
		
		permission = createOdrlPermission("request.resourceUri", "request.permission.assignee", "use");
		policy.add(permission);
		
		addRestrictedNumberConstraint(10, permission);
		addRestrictedDurationConstraint(new Date(), new Date(), permission);
		addDeletionConstraint("after_use", permission);
		
		System.out.println(om.writeValueAsString(policy));
	}
	
	protected static void addDeletionConstraint(String value, OdrlPermission permission) {
		OdrlConstraint item = new OdrlConstraint(
			OdrlOperand.DELETION, OdrlOperator.EQUAL, 
			value
		);
		permission.add(item);
	}

	protected static void addRestrictedNumberConstraint(int maxCount, OdrlPermission permission) {
		OdrlConstraint item = new OdrlConstraint(
			OdrlOperand.COUNT, OdrlOperator.LESS_OR_EQUAL, 
			String.valueOf(maxCount)
		);
		permission.add(item);
	}
	
	protected static void addRestrictedDurationConstraint(Date fromDate, Date toDate, OdrlPermission permission) {
		OdrlConstraint item;
		if(fromDate!=null) {
			item = new OdrlConstraint(
				OdrlOperand.DATETIME, OdrlOperator.GREAT_OR_EQUAL, 
				datetimeToString(fromDate, null)
			);
			permission.add(item);
		}
		
		if(toDate!=null) {
			item = new OdrlConstraint(
				OdrlOperand.DATETIME, OdrlOperator.LESS_OR_EQUAL, 
				datetimeToString(toDate, null)
			);
			permission.add(item);
		}
	}

	protected static OdrlPermission createOdrlPermission(String target, String assignee, String action) {
		OdrlPermission result = new OdrlPermission(target);
		result.setAssignee(new OdrlAssignee(assignee));
		result.addAction(toUsagePolicyActionFullPath(action));
		return result;
	}
	
	protected static String datetimeToString(Date datetime, ZoneId zoneId) {
		if(zoneId==null) {
			zoneId = ZoneId.of("GMT");
		}
		ZonedDateTime zoneDatetime = ZonedDateTime.of(
			LocalDateTime.ofInstant(datetime.toInstant(), zoneId),
			zoneId
		);
		
		DateTimeFormatter formatter;
		// ISO format with the timezone: 2011-12-03T10:15:30+01:00
		formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
		
		// ISO format without the timezone: 2011-12-03T10:15:30
		// formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		
		return zoneDatetime.format(formatter);
	}
	
	protected static String toUsagePolicyActionFullPath(String action) {
		return Odrl.ODRL_NS_PREFIX_V2 + action;
	}

}
