package eu.europa.ec.simpl.sdtoolingbe.service.credential;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

class KeyStoreBuildExceptionTests {
	
	
	@Test
	void testWithMessage() {
		KeyStoreBuildException ex = new KeyStoreBuildException("message");
		assertNull(ex.getCause());
		assertEquals("message", ex.getMessage());
	}
	
	@Test
	void testWithMessageAndCause() {
		Exception cause = new Exception("cause");
		KeyStoreBuildException ex = new KeyStoreBuildException("message", cause);
		assertEquals(cause, ex.getCause());
		assertEquals("message", ex.getMessage());
	}
	
	@Test
	void testWithCause() {
		Exception cause = new Exception("cause");
		KeyStoreBuildException ex = new KeyStoreBuildException(cause);
		assertEquals(cause, ex.getCause());
	}
	
}
