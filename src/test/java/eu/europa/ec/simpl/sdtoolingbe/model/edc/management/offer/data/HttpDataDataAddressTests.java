package eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.data;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class HttpDataDataAddressTests {

    @Test
    void testConstructorDefault() {
        HttpDataDataAddress address = new HttpDataDataAddress();

        assertEquals(HttpDataDataAddress.TYPE_NAME, address.getType(), "Expected type to be set to TYPE_NAME");
        assertNull(address.getBaseUrl(), "Expected baseUrl to be null by default");
    }

    @Test
    void testConstructorWithBaseUrl() {
        String testBaseUrl = "http://example.com";

        HttpDataDataAddress address = new HttpDataDataAddress(testBaseUrl);

        assertEquals(HttpDataDataAddress.TYPE_NAME, address.getType(), "Expected type to be set to TYPE_NAME");
        assertEquals(testBaseUrl, address.getBaseUrl(), "Expected baseUrl to match the provided value");
    }

    @Test
    void testSetters() {
        HttpDataDataAddress address = new HttpDataDataAddress();
        String newBaseUrl = "http://updated-url.com";

        address.setBaseUrl(newBaseUrl);

        assertEquals(newBaseUrl, address.getBaseUrl(), "Expected baseUrl to be updated to the new value");
    }
    
    @Test
    void testToString() {
        HttpDataDataAddress address = new HttpDataDataAddress("http://example.com");
        String expectedToString = "HttpDataDataAddress(baseUrl=http://example.com)";

        assertEquals(expectedToString, address.toString());
    }
}

