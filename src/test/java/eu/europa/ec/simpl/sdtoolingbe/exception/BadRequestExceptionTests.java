package eu.europa.ec.simpl.sdtoolingbe.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class BadRequestExceptionTests {
	
	
	@Test
	void testModel() {
		BadRequestException ex = new BadRequestException("code", "message");
		assertEquals("code", ex.getCode());
		assertEquals("message", ex.getMessage());
	}
	
}
