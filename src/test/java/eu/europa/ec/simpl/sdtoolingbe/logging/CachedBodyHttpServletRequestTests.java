package eu.europa.ec.simpl.sdtoolingbe.logging;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;

class CachedBodyHttpServletRequestTests {
	
	private HttpServletRequest requestMock;
    private CachedBodyHttpServletRequest cachedRequest;

    @BeforeEach
    void setUp() throws IOException {
    	requestMock = mock(HttpServletRequest.class);
    	when(requestMock.getContentLength()).thenReturn(1024);
    	try (MockServletInputStream mockInputStream = new MockServletInputStream("test body")) {
    	    when(requestMock.getInputStream()).thenReturn(mockInputStream);
    	    when(requestMock.getCharacterEncoding()).thenReturn("UTF-8");
    	    cachedRequest = new CachedBodyHttpServletRequest(requestMock);
    	}

    }

    @Test
    void testGetInputStream() throws IOException {
    	String expectedContent = "test body";
    	int expectedBytesCount= expectedContent.getBytes().length;
    	ServletInputStream inputStream = cachedRequest.getInputStream();
    	ServletInputStream inputStream2 = cachedRequest.getInputStream();
    	
    	assertEquals(inputStream, inputStream2);
    	
    	byte[] content = new byte[expectedContent.getBytes().length];
        int readBytes = inputStream.read(content);
        
        assertEquals(expectedBytesCount, readBytes);
        assertEquals(expectedContent, new String(content));
    }

    @Test
    void testGetCharacterEncoding() {
        assertEquals("UTF-8", cachedRequest.getCharacterEncoding());
    }

    @Test
    void testGetParameter() {
        when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameter("param")).thenReturn("value");

        String param = cachedRequest.getParameter("param");
        assertEquals("value", param);
    }

    @Test
    void testGetParameterMap() {
        when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(
        	Map.of(
        		"param1", new String[]{"value1"},
        		"param2", new String[]{"value2"}
        	)
        );

        Map<String, String[]> paramMap = cachedRequest.getParameterMap();
        assertTrue(paramMap.containsKey("param1"));
        assertArrayEquals(new String[]{"value1"}, paramMap.get("param1"));
        assertTrue(paramMap.containsKey("param2"));
        assertArrayEquals(new String[]{"value2"}, paramMap.get("param2"));
    }

    @Test
    void testGetContentAsByteArray() throws IOException {
    	when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        cachedRequest.getInputStream();
        cachedRequest.getParameterNames();	// load the cached content
        byte[] content = cachedRequest.getContentAsByteArray();
        String result = new String(content);
        assertEquals("param=value", result);
    }
    
    @Test
    void testConstructorWithContentCacheLimit() throws IOException {
    	cachedRequest = new CachedBodyHttpServletRequest(requestMock, 2048);
    	when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        cachedRequest.getInputStream();
        cachedRequest.getParameterNames();	// load the cached content
    	
        byte[] content = cachedRequest.getContentAsByteArray();
        String result = new String(content);
        assertEquals("param=value", result);
    }
    
    @Test
    void testHandleContentOverflow() throws IOException {
    	cachedRequest = new CachedBodyHttpServletRequest(requestMock, 128);
    	when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        cachedRequest.getInputStream();
        cachedRequest.getParameterNames();	// load the cached content
    	
        byte[] content = cachedRequest.getContentAsByteArray();
        String result = new String(content);
        assertEquals("param=value", result);
    }
    
    @Test
    void testGetReader() throws IOException {
    	when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        cachedRequest.getInputStream();
        cachedRequest.getParameterNames();	// load the cached content
    	
        BufferedReader br = cachedRequest.getReader();
        BufferedReader br2 = cachedRequest.getReader();
        
        assertEquals(br, br2);
    }
    
    @Test
    void testGetParameterValues() throws IOException {
    	when(requestMock.getContentType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestMock.getMethod()).thenReturn("POST");
        when(requestMock.getParameterMap()).thenReturn(Collections.singletonMap("param", new String[]{"value"}));
        when(requestMock.getParameterValues(anyString())).thenReturn(new String[]{"value"});
        cachedRequest.getInputStream();
    	
        String[] values = cachedRequest.getParameterValues("param");
        String[] values2 = cachedRequest.getParameterValues("param");
        
        assertNotNull(values);
        assertNotNull(values2);
        assertEquals(1, values.length);
        assertEquals(1, values2.length);
        assertEquals(values[0], values2[0]);
    }

    // Custom mock ServletInputStream for testing purposes
    static class MockServletInputStream extends ServletInputStream {
        private final ByteArrayInputStream bais;

        public MockServletInputStream(String body) {
            this.bais = new ByteArrayInputStream(body.getBytes());
        }

        @Override
        public int read() throws IOException {
            return bais.read();
        }

        @Override
        public boolean isFinished() {
            return bais.available() == 0;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setReadListener(ReadListener readListener) {
            // No-op
        }
    }
}
