package eu.europa.ec.simpl.sdtoolingbe.util.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import com.auth0.jwt.JWT;

import eu.europa.ec.simpl.sdtoolingbe.TestSupport;

class AuthBearerUtilTests {
    
    private static final String VALID_JWT = TestSupport.createValidJwt();
	
	private String payload;
	private String credentialId;
	
	public AuthBearerUtilTests() {
		payload = new String(Base64.getDecoder().decode(JWT.decode(VALID_JWT).getPayload()));
		JSONObject payloadObj = new JSONObject(payload);
		credentialId = payloadObj.getString("credential_id");
		assertTrue(StringUtils.isNotBlank(credentialId));
	}
	
	
    @Test
    void testGetBearerValueWithBearerToken() {
        // Simula la richiesta con un token Bearer nell'header Authorization
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", AuthBearerUtil.toBearerString("sampleJwtToken"));

        String result = AuthBearerUtil.getBearerValue(request);
        assertEquals("sampleJwtToken", result);
    }

    @Test
    void testGetBearerValueNoBearerToken() {
        // Simula la richiesta senza header Authorization
        MockHttpServletRequest request = new MockHttpServletRequest();

        String result = AuthBearerUtil.getBearerValue(request);
        assertNull(result);
    }

    @Test
    void testGetBearerValueInvalidBearerToken() {
        // Simula la richiesta con un Authorization header invalido
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "InvalidToken");

        String result = AuthBearerUtil.getBearerValue(request);
        assertNull(result);
    }

    @Test
    void testGetJwtTokenPayloadAsJsonObjectWithValidPayload() {
        // Simula una richiesta con un token JWT valido contenente un payload JSON codificato
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", AuthBearerUtil.toBearerString(VALID_JWT));

        JSONObject result = AuthBearerUtil.getJwtTokenPayloadAsJsonObject(request);
        assertNotNull(result);
        assertEquals(credentialId, result.getString("credential_id"));
    }

    @Test
    void testGetJwtTokenPayloadAsJsonObjectNoPayload() {
        // Simula una richiesta senza token JWT
        MockHttpServletRequest request = new MockHttpServletRequest();

        JSONObject result = AuthBearerUtil.getJwtTokenPayloadAsJsonObject(request);
        assertNull(result);
    }

    @Test
    void testGetJwtTokenPayloadWithValidJwtToken() {
        // Simula una richiesta con un token JWT contenente un payload JSON codificato
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", AuthBearerUtil.toBearerString(VALID_JWT));

        String result = AuthBearerUtil.getJwtTokenPayload(request);
        assertNotNull(result);
        assertEquals(payload, result);
    }

    @Test
    void testGetJwtTokenPayloadNoJwtToken() {
        // Simula una richiesta senza token JWT
        MockHttpServletRequest request = new MockHttpServletRequest();

        String result = AuthBearerUtil.getJwtTokenPayload(request);
        assertNull(result);
    }

    @Test
    void testGetCredentialIdWithValidCredentialId() {
        // Simula una richiesta con un token JWT contenente un payload JSON con "credential_id"
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", AuthBearerUtil.toBearerString(VALID_JWT));

        String result = AuthBearerUtil.getCredentialId(request);
        assertEquals(credentialId, result);
    }

    @Test
    void testGetCredentialIdNoCredentialId() {
        // Simula una richiesta con un token JWT senza "credential_id" nel payload
        MockHttpServletRequest request = new MockHttpServletRequest();
        String header = Base64.getEncoder().encodeToString("{\"key\":\"value\"}".getBytes());
        String payload = Base64.getEncoder().encodeToString("{\"key\":\"value\"}".getBytes());
        String signature = Base64.getEncoder().encodeToString("{\"key\":\"value\"}".getBytes());
        String jwtToken = header + "." + payload + "." + signature;
        request.addHeader("Authorization", AuthBearerUtil.toBearerString(jwtToken));

        String result = AuthBearerUtil.getCredentialId(request);
        assertNull(result);
    }

    @Test
    void testGetCredentialIdNoJwtToken() {
        // Simula una richiesta senza token JWT
        MockHttpServletRequest request = new MockHttpServletRequest();

        String result = AuthBearerUtil.getCredentialId(request);
        assertNull(result);
    }

}
