package eu.europa.ec.simpl.sdtoolingbe.service.usersroles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.europa.ec.simpl.sdtoolingbe.client.edcconnector.EDCConnectorClient;
import eu.europa.ec.simpl.sdtoolingbe.client.usersroles.UsersRolesClient;
import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import eu.europa.ec.simpl.sdtoolingbe.model.client.identityattribute.IdentityAttribute;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;
import feign.FeignException;

class UsersRolesServiceTests {
	
	@Mock
    private UsersRolesClient usersRolesClient;

    @InjectMocks
    private UsersRolesServiceImpl usersRolesService;
	
	
	@Mock
	private EDCConnectorClient edcConnectorClient;
	
	@BeforeEach
	void setUpEach() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
    void testGetConsumerIdentityAttributesWithConsumerParticipant() throws Exception {
        // Preparazione dati di test JSON simulando il JSON di risposta
        JSONArray jsonArray = new JSONArray();
        JSONObject consumerAttribute = new JSONObject();
        consumerAttribute.put("code", "consumerCode");
        consumerAttribute.put("name", "consumerName");
        consumerAttribute.put("participantTypes", new JSONArray(List.of("CONSUMER")));
        jsonArray.put(consumerAttribute);

        JSONObject nonConsumerAttribute = new JSONObject();
        nonConsumerAttribute.put("code", "nonConsumerCode");
        nonConsumerAttribute.put("name", "nonConsumerName");
        nonConsumerAttribute.put("participantTypes", new JSONArray(List.of("OTHER")));
        jsonArray.put(nonConsumerAttribute);

        String jsonResponse = jsonArray.toString();
        
        // Mock del metodo usersRolesClient.getAgentIdentityAttributes
        when(usersRolesClient.getAgentIdentityAttributes(any(String.class))).thenReturn(jsonResponse);

        // Invocazione del metodo da testare
        List<IdentityAttribute> result = usersRolesService.getIdentityAttributes(AuthBearerUtil.toBearerString("testToken"), "CONSUMER");

        // Asserzioni
        assertEquals(1, result.size());
        assertEquals(consumerAttribute.get("code"), result.get(0).getCode());
        assertEquals(consumerAttribute.get("name"), result.get(0).getIdentifier());
    }

    @Test
    void testGetConsumerIdentityAttributesNoConsumerParticipant() throws Exception {
        // Preparazione di un JSON senza alcun partecipante di tipo "CONSUMER"
        JSONArray jsonArray = new JSONArray();
        JSONObject nonConsumerAttribute = new JSONObject();
        nonConsumerAttribute.put("code", "nonConsumerCode");
        nonConsumerAttribute.put("name", "nonConsumerName");
        nonConsumerAttribute.put("participantTypes", new JSONArray(List.of("OTHER")));
        jsonArray.put(nonConsumerAttribute);

        String jsonResponse = jsonArray.toString();

        // Mock del metodo usersRolesClient.getAgentIdentityAttributes
        when(usersRolesClient.getAgentIdentityAttributes(any(String.class))).thenReturn(jsonResponse);

        // Invocazione del metodo da testare
        List<IdentityAttribute> result = usersRolesService.getIdentityAttributes(AuthBearerUtil.toBearerString("testToken"), "CONSUMER");

        // Asserzioni
        assertEquals(0, result.size());
    }
    
    @Test
    void testGetConsumerIdentityAttributesNoParticipantTypes() throws Exception {
        // Preparazione di un JSON senza alcun partecipante di tipo "CONSUMER"
        JSONArray jsonArray = new JSONArray();
        JSONObject nonConsumerAttribute = new JSONObject();
        nonConsumerAttribute.put("code", "nonConsumerCode");
        nonConsumerAttribute.put("name", "nonConsumerName");
        jsonArray.put(nonConsumerAttribute);

        String jsonResponse = jsonArray.toString();

        // Mock del metodo usersRolesClient.getAgentIdentityAttributes
        when(usersRolesClient.getAgentIdentityAttributes(any(String.class))).thenReturn(jsonResponse);

        // Invocazione del metodo da testare
        List<IdentityAttribute> result = usersRolesService.getIdentityAttributes(AuthBearerUtil.toBearerString("testToken"), "CONSUMER");

        // Asserzioni
        assertEquals(0, result.size());
    }
    
    @Test
    void testGetConsumerIdentityAttributesWithUnauthorizedException() {
        when(usersRolesClient.getAgentIdentityAttributes(any(String.class))).thenThrow(FeignException.Unauthorized.class);

        assertThrows(UnauthorizedException.class, () -> usersRolesService.getIdentityAttributes(AuthBearerUtil.toBearerString("testToken"), "CONSUMER"));
    }
	
}