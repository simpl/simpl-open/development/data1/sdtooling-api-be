package eu.europa.ec.simpl.sdtoolingbe.service.hashservice;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.europa.ec.simpl.sdtoolingbe.TestSupport;
import eu.europa.ec.simpl.sdtoolingbe.dto.HashModel;
import eu.europa.ec.simpl.sdtoolingbe.exception.BadRequestException;
import eu.europa.ec.simpl.sdtoolingbe.properties.HashProperties;
import eu.europa.ec.simpl.sdtoolingbe.properties.ValidationProperties;
import eu.europa.ec.simpl.sdtoolingbe.service.validationService.ValidationServiceImpl;
import eu.europa.ec.simpl.sdtoolingbe.util.HashGeneratorUtil;

class HashServiceTests {


    @Mock
    private ValidationProperties validationProperties;

    @Mock
    private ValidationServiceImpl validationService;

    private HashService hashService;

    @Mock
    private HashProperties hashProperties;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);

        when(hashProperties.getHashAlgorithm()).thenReturn("SHA-256");
        when(hashProperties.getHashModelList()).thenReturn(List.of(new HashModel()));
        when(validationProperties.isEnabled()).thenReturn(false);

        HashGeneratorUtil hashGeneratorUtil = new HashGeneratorUtil(hashProperties);
        hashService = new HashServiceImpl(hashProperties, hashGeneratorUtil);
    }


    @Test
    void testGenerateHashFromJsonLdWithValidJsonLd() throws Exception {
        String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-infra-offering.json", null);

        String ecosystem = "simpl";

        when(hashProperties.getValueFromKey("testDocument")).thenReturn("http://example.com/test");

        String result = hashService.generateHashFromJsonLd(sdJsonLd, ecosystem);

        assertNotNull(result);
        assertTrue(result.contains("\"simpl:generalServiceProperties\""));
    }

    @Test
    void testGenerateHashFromJsonLdWithInvalidJsonLd() {
        String invalidJsonLd = "invalid json";
        String ecosystem = "example";

        Exception exception = assertThrows(BadRequestException.class, () -> hashService.generateHashFromJsonLd(invalidJsonLd, ecosystem));

        assertTrue(exception.getMessage().contains("Input Json-Ld not valid"));
    }

    @Test
    void testGenerateHashFromJsonLdWithmissingHashObject() throws Exception {
        String jsonLd = "{ \"example:someOtherObj\": {} }";
        String ecosystem = "example";

        String result = hashService.generateHashFromJsonLd(jsonLd, ecosystem);

        assertNotNull(result);
        assertFalse(result.contains("example:hashAlg"));
        assertFalse(result.contains("example:hashValue"));
    }

    /* non si riesce a mokkare la richiesta
    @Test
    void testGenerateHashValueWithvalidDocument() throws Exception {
    	String document = "testDocument";
        String url = "http://example.com/test";

        // Mock del valore restituito da hashProperties
        when(hashProperties.getValueFromKey(document)).thenReturn(url);

        // Mock della connessione HTTP
        HttpURLConnection mockConnection = mock(HttpURLConnection.class);
        URL mockURL = mock(URL.class);

        // Simula la creazione della connessione
        when(mockURL.openConnection()).thenReturn(mockConnection);
        //whenNew(URL.class).withArguments(url).thenReturn(mockURL);

        // Simula il codice di risposta OK
        when(mockConnection.getResponseCode()).thenReturn(HttpURLConnection.HTTP_OK);

        // Simula lo stream di input restituito dalla URL
        InputStream mockStream = new ByteArrayInputStream("testData".getBytes());
        when(mockURL.openStream()).thenReturn(mockStream);

        // Esegui il metodo
        String hash = hashService.generateHashValue(document);

        // Verifica che il valore restituito sia valido
        assertNotNull(hash);
        assertFalse(hash.isEmpty());

        // Verifica che le chiamate corrette siano avvenute
        verify(mockConnection).getResponseCode();
        verify(mockStream).close();
    }
    */

}
