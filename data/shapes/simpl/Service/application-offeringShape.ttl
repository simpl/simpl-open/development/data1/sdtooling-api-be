@prefix gax-validation: <http://w3id.org/gaia-x/validation#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix simpl: <http://w3id.org/gaia-x/simpl#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

gax-validation:ApplicationOfferingShape a sh:NodeShape ;
    sh:property [ skos:example "'(a structure object of type ApplicationProperties'" ;
            sh:description "Basic classification of the service as per provision type and service model." ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "application properties" ;
            sh:node gax-validation:ApplicationPropertiesShape ;
            sh:order 1 ;
            sh:path simpl:applicationProperties ],
        [ skos:example "'(a structure object of type GeneralServiceProperties'" ;
            sh:description "Basic classification of the service as per provision type and service model." ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "general service properties" ;
            sh:node gax-validation:GeneralServicePropertiesShape ;
            sh:order 1 ;
            sh:path simpl:generalServiceProperties ],
        [ skos:example "'(a structure object of type ProviderInformation'" ;
            sh:description "Basic classification of the service as per provision type and service model." ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "provider information" ;
            sh:node gax-validation:ProviderInformationShape ;
            sh:order 2 ;
            sh:path simpl:providerInformation ],
        [ skos:example "'(a structure object of type OfferingPrice'" ;
            sh:description "Basic classification of the service as per provision type and service model." ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "offering price" ;
            sh:node gax-validation:OfferingPriceShape ;
            sh:order 3 ;
            sh:path simpl:offeringPrice ],
        [ skos:example "'(a structure object of type ServicePolicy'" ;
            sh:description "Basic classification of the service as per provision type and service model." ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "service policy" ;
            sh:node gax-validation:ServicePolicyShape ;
            sh:order 4 ;
            sh:path simpl:servicePolicy ],
        [ skos:example "'(a structure object of type ContractTemplate'" ;
            sh:description "Basic classification of the service as per provision type and service model." ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "contract template" ;
            sh:node gax-validation:ContractTemplateShape ;
            sh:order 5 ;
            sh:path simpl:contractTemplate ],
        [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'(a structure object of type EdcRegistration'" ;
            sh:description "Basic classification of the service as per provision type and service model." ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "edc registration" ;
            sh:node gax-validation:EdcRegistrationShape ;
            sh:order 6 ;
            sh:path simpl:edcRegistration ],
        [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'(a structure object of type EdcConnector'" ;
            sh:description "Basic classification of the service as per provision type and service model." ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "edc connector" ;
            sh:node gax-validation:EdcConnectorShape ;
            sh:order 7 ;
            sh:path simpl:edcConnector ] ;
    sh:targetClass simpl:ApplicationOffering .

gax-validation:ApplicationPropertiesShape a sh:NodeShape ;
    sh:property [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'Text'" ;
            sh:datatype xsd:string ;
            sh:description "Application package" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "package" ;
            sh:order 1 ;
            sh:path simpl:package ;
            sh:pattern "^[a-zA-Z0-9][a-zA-Z0-9\\s]*$" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'www.gx.com'" ;
            sh:datatype xsd:anyURI ;
            sh:description "Application link" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "link" ;
            sh:order 2 ;
            sh:path simpl:link ;
            sh:pattern "[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,15}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'docker image, executable package'" ;
            sh:datatype xsd:anyURI ;
            sh:description "Format under which the application is distributed (e.g. docker image, executable package, ...). Might be more than one format available for the same application." ;
            sh:name "open api" ;
            sh:order 3 ;
            sh:path simpl:openApi ;
            sh:pattern "[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,15}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'Text'" ;
            sh:datatype xsd:string ;
            sh:description "Target users" ;
            sh:name "target users" ;
            sh:order 4 ;
            sh:path simpl:targetUsers ;
            sh:pattern "^[a-zA-Z0-9][a-zA-Z0-9\\s]*$" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'Text'" ;
            sh:datatype xsd:string ;
            sh:description "Requirements on the runtime environment (useful for the deployment in the infrastructure provider)" ;
            sh:name "requirements" ;
            sh:order 5 ;
            sh:path simpl:requirements ;
            sh:pattern "^[a-zA-Z0-9][a-zA-Z0-9\\s]*$" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'www.gx.com'" ;
            sh:datatype xsd:anyURI ;
            sh:description "URL of the documentation on how to use the application" ;
            sh:name "url" ;
            sh:order 6 ;
            sh:path simpl:url ;
            sh:pattern "[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,15}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)" ],
        [ simpl:configure ( "requiredOnFrontendOnly" ) ;
            skos:example "'{ 'type': 'HttpData', 'baseUrl': 'http://localhost:8080/test' }'" ;
            sh:datatype xsd:string ;
            sh:description "Provider Data address" ;
            sh:maxCount 1 ;
            sh:name "provider data address" ;
            sh:order 7 ;
            sh:path simpl:providerDataAddress ] ;
    sh:targetClass simpl:ApplicationProperties .

gax-validation:ContractTemplateShape a sh:NodeShape ;
    sh:property [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'Contract Template 1', 'Contract Template 2'" ;
            sh:datatype xsd:string ;
            sh:description "Indicates Contract Template Document" ;
            sh:in ( "Contract Template 1" "Contract Template 2" "Contract Template 3" ) ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "contract template document" ;
            sh:order 1 ;
            sh:path simpl:contractTemplateDocument ],
        [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'Free text'" ;
            sh:datatype xsd:string ;
            sh:description "Indicates HASH VALUE" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "contract template hash value" ;
            sh:order 2 ;
            sh:path simpl:contractTemplateHashValue ],
        [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'Free text'" ;
            sh:datatype xsd:string ;
            sh:description "Indicates HASH ALGORITHM" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "contract template hash alg" ;
            sh:order 3 ;
            sh:path simpl:contractTemplateHashAlg ],
        [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'URL'" ;
            sh:datatype xsd:string ;
            sh:description "URL" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "contract template u r l" ;
            sh:order 4 ;
            sh:path simpl:contractTemplateURL ] ;
    sh:targetClass simpl:ContractTemplate .

gax-validation:EdcConnectorShape a sh:NodeShape ;
    sh:property [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'providerEndpointURL'" ;
            sh:datatype xsd:string ;
            sh:description "Provider endpoint url" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "provider endpoint u r l" ;
            sh:order 1 ;
            sh:path simpl:providerEndpointURL ] ;
    sh:targetClass simpl:EdcConnector .

gax-validation:EdcRegistrationShape a sh:NodeShape ;
    sh:property [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'Identifier'" ;
            sh:datatype xsd:string ;
            sh:description "Unique identifier of contract definition" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "contract definition id" ;
            sh:order 1 ;
            sh:path simpl:contractDefinitionId ],
        [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'Identifier'" ;
            sh:datatype xsd:string ;
            sh:description "Unique identifier of service policy" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "service policy id" ;
            sh:order 2 ;
            sh:path simpl:servicePolicyId ],
        [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'Identifier'" ;
            sh:datatype xsd:string ;
            sh:description "Unique identifier of access policy" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "access policy id" ;
            sh:order 3 ;
            sh:path simpl:accessPolicyId ],
        [ simpl:configure ( "hiddenInFrontend" ) ;
            skos:example "'Identifier'" ;
            sh:datatype xsd:string ;
            sh:description "Unique identifier of asset" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "asset id" ;
            sh:order 4 ;
            sh:path simpl:assetId ] ;
    sh:targetClass simpl:EdcRegistration .

gax-validation:GeneralServicePropertiesShape a sh:NodeShape ;
    sh:property [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'Title'" ;
            sh:datatype xsd:string ;
            sh:description "A short text title for the dataset/application/infrastructure" ;
            sh:maxCount 1 ;
            sh:maxLength 255 ;
            sh:minCount 1 ;
            sh:minLength 1 ;
            sh:name "name" ;
            sh:order 1 ;
            sh:path simpl:name ;
            sh:pattern "^(.|\\s)*[a-zA-Z]+(.|\\s)*$" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'Description'" ;
            sh:datatype xsd:string ;
            sh:description "A description for the dataset/application/infrastructure" ;
            sh:maxCount 1 ;
            sh:maxLength 1000 ;
            sh:minCount 1 ;
            sh:minLength 1 ;
            sh:name "description" ;
            sh:order 2 ;
            sh:path simpl:description ;
            sh:pattern "^(.|\\s)*[a-zA-Z]+(.|\\s)*$" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'URL, handle'" ;
            sh:datatype xsd:anyURI ;
            sh:description "The location where the dataset/application/infrastructure can be found (likely an endpoint URL)" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "service access point" ;
            sh:order 3 ;
            sh:path simpl:serviceAccessPoint ;
            sh:pattern "[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,15}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'Keywords'" ;
            sh:datatype xsd:string ;
            sh:description "Keywords" ;
            sh:maxCount 16 ;
            sh:maxLength 50 ;
            sh:minLength 1 ;
            sh:name "keywords" ;
            sh:order 4 ;
            sh:path simpl:keywords ;
            sh:pattern "^(.|\\s)*[a-zA-Z]+(.|\\s)*$" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'EN, IT'" ;
            sh:datatype xsd:string ;
            sh:description "Language (of the metadata, like the title, description)" ;
            sh:in ( "bg" "hr" "cs" "da" "nl" "en" "et" "fi" "fr" "de" "el" "hu" "ga" "it" "lv" "lt" "mt" "pl" "pt" "ro" "sk" "sl" "es" "sv" ) ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "in language" ;
            sh:order 5 ;
            sh:path simpl:inLanguage ],
        [ simpl:configure ( "hiddenInFrontend" "useForAdvancedSearch" ) ;
            skos:example "'application, data, infrastructure'" ;
            sh:datatype xsd:string ;
            sh:description "Offering type using for search" ;
            sh:in ( "application" "data" "infrastructure" ) ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "offering type" ;
            sh:order 6 ;
            sh:path simpl:offeringType ] ;
    sh:targetClass simpl:GeneralServiceProperties .

gax-validation:OfferingPriceShape a sh:NodeShape ;
    sh:property [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'0, 1, 2'" ;
            sh:datatype xsd:decimal ;
            sh:description "Price and conditions" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:minInclusive 0.0 ;
            sh:name "price" ;
            sh:order 3 ;
            sh:path simpl:price ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'free, commercial'" ;
            sh:datatype xsd:string ;
            sh:description "Price Type" ;
            sh:in ( "free" "commercial" ) ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "price type" ;
            sh:order 4 ;
            sh:path simpl:priceType ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'www.simpl.com'" ;
            sh:datatype xsd:anyURI ;
            sh:description "URL to the used license" ;
            sh:maxLength 255 ;
            sh:minCount 1 ;
            sh:minLength 1 ;
            sh:name "license" ;
            sh:order 1 ;
            sh:path simpl:license ;
            sh:pattern "[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,15}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'BGN, EUR, CZK'" ;
            sh:datatype xsd:string ;
            sh:description "Price and conditions" ;
            sh:in ( "BGN" "EUR" "CZK" "DKK" "HUF" "PLN" "RON" "SEK" ) ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "currency" ;
            sh:order 2 ;
            sh:path simpl:currency ] ;
    sh:targetClass simpl:OfferingPrice .

gax-validation:ProviderInformationShape a sh:NodeShape ;
    sh:property [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'123'" ;
            sh:datatype xsd:string ;
            sh:description "A reference to ID of the dataset/application/infrastructure provider" ;
            sh:maxCount 1 ;
            sh:maxLength 255 ;
            sh:minCount 1 ;
            sh:minLength 1 ;
            sh:name "provided by" ;
            sh:order 1 ;
            sh:path simpl:providedBy ;
            sh:pattern "^(.|\\s)*[a-zA-Z]+(.|\\s)*$" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'support'" ;
            sh:datatype xsd:string ;
            sh:description "Who to contact in case of questions/issues" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "contact" ;
            sh:order 2 ;
            sh:path simpl:contact ;
            sh:pattern "^[\\w\\-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'signature'" ;
            sh:datatype xsd:string ;
            sh:description "A digital signature that ensures that the provider is the one that uploaded the Self-description" ;
            sh:maxCount 1 ;
            sh:minCount 1 ;
            sh:name "signature" ;
            sh:order 3 ;
            sh:path simpl:signature ;
            sh:pattern "^[a-zA-Z0-9][a-zA-Z0-9\\s]*$" ] ;
    sh:targetClass simpl:ProviderInformation .

gax-validation:ServicePolicyShape a sh:NodeShape ;
    sh:property [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'ODRL policy'" ;
            sh:datatype xsd:string ;
            sh:description "Access policy to define who can access the dataset" ;
            sh:minCount 1 ;
            sh:name "access-policy" ;
            sh:order 1 ;
            sh:path simpl:access-policy ;
            sh:pattern "[:,\\{\\}\\[\\]]|(\".*?\")|('.*?')|[-\\w.]+" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'ODRL policy'" ;
            sh:datatype xsd:string ;
            sh:description "Usage policy to define how a dataset can be used" ;
            sh:minCount 1 ;
            sh:name "usage-policy" ;
            sh:order 2 ;
            sh:path simpl:usage-policy ;
            sh:pattern "[:,\\{\\}\\[\\]]|(\".*?\")|('.*?')|[-\\w.]+" ],
        [ simpl:configure ( "useForAdvancedSearch" ) ;
            skos:example "'Free text'" ;
            sh:datatype xsd:string ;
            sh:description "Indicates compliance with relevant data protection regulations and standards" ;
            sh:name "data protection regime" ;
            sh:order 3 ;
            sh:path simpl:dataProtectionRegime ;
            sh:pattern "[:,\\{\\}\\[\\]]|(\".*?\")|('.*?')|[-\\w.]+" ] ;
    sh:targetClass simpl:ServicePolicy .

